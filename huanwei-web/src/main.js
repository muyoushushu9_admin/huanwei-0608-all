import Vue from 'vue'

import Cookies from 'js-cookie'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'//引入element-ui css 文件
import './assets/styles/element-variables.scss'
import '@/assets/styles/index.scss' // global css
import '@/assets/styles/ruoyi.scss' // ruoyi css

import '@/assets/font/index.css';  //字体
import Viewer from 'v-viewer'//图片预览
import 'default-passive-events'//
import 'viewerjs/dist/viewer.css';
Vue.use(Viewer, {
  defaultOptions: {
    zIndex: 9999,//解决图片放大的层级问题
  }
});


import App from './App'
import store from './store'
import router from './router'
import directive from './directive' // directive
import plugins from './plugins' // plugins
import { download } from '@/utils/request'
import { formReset } from '@/utils/index'
import './assets/icons' // icon
import './permission' // permission control
import { getDicts } from "@/api/system/dict/data";
import { getConfigKey, updateConfigByKey } from "@/api/system/config";
import { parseTime, resetForm, addDateRange, selectDictLabel, selectDictLabels, handleTree, generateUUID } from "@/utils/ruoyi";
import formRules from '@/utils/rules.js';

import common from './assets/js/common'

import subpagePermissions from '@/utils/subpagePermissions/js.js'
Vue.use(common);
import VueMention from 'vue-mention';

Vue.use(VueMention)
import vueEsign from 'vue-esign'
Vue.use(vueEsign)
// 分页组件
import Pagination from "@/components/Pagination";
// 搜索条件组件
import SearchCriteria from "@/components/SearchCriteria"
// 自定义表格工具组件
import RightToolbar from "@/components/RightToolbar"
// 富文本组件
import Editor from "@/components/Editor"
// 文件上传组件
import FileUpload from "@/components/FileUpload"
import FileUploads from "@/components/FileUpload"
// 视频上传组件
import FileVideo from "@/components/FileVideo"
// 图片上传组件
import ImageUpload from "@/components/ImageUpload"
// 图片预览组件
import ImagePreview from "@/components/ImagePreview"
// 字典标签组件
import DictTag from '@/components/DictTag'
// 头部标签组件
import VueMeta from 'vue-meta'
// 字典数据组件
import DictData from '@/components/DictData'

import Affix from '@/components/Affix';

import returnFormId from '@/utils/returnFormId.js';
import '@/utils/draggable.js';
//bpmn.js开始
import { vuePlugin } from "@/components/Bpmn/package/highlight/index.js";
import "highlight.js/styles/atom-one-dark-reasonable.css";
Vue.use(vuePlugin);

import MyPD from "@/components/Bpmn/package/index.js";
Vue.use(MyPD);
import "@/components/Bpmn/package/theme/index.scss";

import "bpmn-js/dist/assets/diagram-js.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css";
import Plugin from 'v-fit-columns';
Vue.use(Plugin);
// 甘特图部分
import wlGantt from 'wl-gantt'
import "wl-gantt/lib/wl-gantt.css"
Vue.use(wlGantt)
//bpmn.js结束
import AMap from 'vue-amap'
Vue.use(AMap)
import VEmojiPicker from 'v-emoji-picker';

Vue.config.productionTip = false;
Vue.use(VEmojiPicker);
// IndexDB封装类库 https://localforage.github.io/localForage/#installation
import localforage from 'localforage'

Vue.use(localforage)

// 将 localforage 挂载到全局示例, 这样就可以在任何地方 用 this.$localforage 操作
Vue.prototype.localforage = localforage

console.info('localforage初始化成功，使用 this.$localforage 调用')

// 创建一个 默认的 IndexDB数据库挂载到全局
const demoDataBase = localforage.createInstance({
  name: 'demoDataBase'
})

Vue.prototype.$demoDataBase = demoDataBase
console.info('默认数据库 demoDataBase  初始化成功，使用 this.$demoDataBase 调用')

Vue.config.productionTip = false

import Contextmenu from "vue-contextmenujs"//自定义右键菜单
Vue.use(Contextmenu);
// 初始化vue-amap
AMap.initAMapApiLoader({
  // 高德key
  key: '92bf1b30d5d094e6a625fd046e52f2db',
  // 插件集合 （插件按需引入）
  plugin: [
    'AMap.DistrictSearch',
    'AMap.Autocomplete', // 输入提示插件
    'AMap.PlaceSearch', // POI搜索插件
    'AMap.Scale', // 右下角缩略图插件 比例尺
    'AMap.OverView', // 地图鹰眼插件
    'AMap.ToolBar', // 地图工具条
    'AMap.MapType', // 类别切换控件，实现默认图层与卫星图、实施交通图层之间切换的控制
    'AMap.PolyEditor', // 编辑 折线多，边形
    'AMap.CircleEditor', // 圆形编辑器插件
    'AMap.Geolocation', // 定位控件，用来获取和展示用户主机所在的经纬度位置
    'AMap.Geocoder'
  ],
  v: '1.4.4'
});

import adaptive from '@/utils/el-table'
Vue.use(adaptive)
const bus = new Vue({})
// 全局方法挂载
Vue.prototype.getDicts = getDicts
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.updateConfigByKey = updateConfigByKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.selectDictLabels = selectDictLabels
Vue.prototype.download = download
Vue.prototype.handleTree = handleTree
Vue.prototype.formRules = formRules
Vue.prototype.generateUUID = generateUUID
Vue.prototype.formReset = formReset;
Vue.prototype.$bus = bus;
Vue.prototype.$tableDisplayDecide = subpagePermissions.authorityJudgment
Vue.prototype.returnFormId = returnFormId


// 全局组件挂载
Vue.component('DictTag', DictTag)
Vue.component('Pagination', Pagination)
Vue.component('SearchCriteria', SearchCriteria)
Vue.component('RightToolbar', RightToolbar)
Vue.component('Editor', Editor)
Vue.component('FileUpload', FileUpload)
Vue.component('FileUploads', FileUploads)
Vue.component('FileVideo', FileVideo)
Vue.component('ImageUpload', ImageUpload)
Vue.component('ImagePreview', ImagePreview)
Vue.component('Affix', Affix)



Vue.use(directive)
Vue.use(plugins)
Vue.use(VueMeta)
DictData.install()

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

// 修改 el-dialog 默认点击遮照为不关闭
Element.Dialog.props.closeOnClickModal.default = false

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
