// 导出页面为PDF格式
import html2Canvas from 'html2canvas'
import JsPDF from 'jspdf'
export default function (file_title, dom) {
        const oneDom = document.querySelector(dom);
      const title = file_title
      const Dom_height = oneDom.height() //获取要截取的dom元素内容高度
      const outer_height = oneDom.outerHeight() //获取dom元素的外部高度
      const window_height = window.screen.availHeight //窗口高度
      html2Canvas(document.querySelector(dom), {
        allowTaint: true,
        // height: window.screen.availHeight,
        height: Dom_height < window_height ? window_height : outer_height, //canvas画布的具体高度
        /*以上这个高度非常重要，如果dom内容高度在窗口高度内的话，那就用窗口高度，
        确保把当前页的内容都截取，如果超过一页，那就用外部高度*/
        windowHeight: document.querySelector(dom).scrollHeight,
        y: pageYOffset || 0 //页面在垂直方向的滚动距离  window.pageYOffset - 30
      }).then(function (canvas) {
            let contentWidth = canvas.width
            let contentHeight = canvas.height
            let pageHeight = contentWidth / 592.28 * 841.89
            let leftHeight = contentHeight
            let position = 0
            let imgWidth = 595.28
            let imgHeight = 592.28 / contentWidth * contentHeight
            let pageData = canvas.toDataURL('image/jpeg', 1.0)
            let PDF = new JsPDF('', 'pt', 'a4')
            if (leftHeight < pageHeight) {
              PDF.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight)
            } else {
              while (leftHeight > 0) {
                PDF.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight)
                leftHeight -= pageHeight
                position -= 841.89
                if (leftHeight > 0) {
                  PDF.addPage()
                }
              }
            }
            PDF.save(title + '.pdf')
          }
      )
    };