import { customerGetForm } from "@/api/workflows/customerManag.js"; // 客户管理
import { excItemGetForm } from "@/api/mochaItom/exceptions.js"; // 异常项管理
import { partnerLinkmanGetForm } from "@/api/partner/contacts.js"; // 联系人列表
import { projectListGetForm } from "@/api/workflows/projectList.js"; // 项目列表
import { equipmentListGetForm } from "@/api/equipmentList/index.js"; //  设备列表
import { bomManagementGetForm } from "@/api/equipmentList/bom.js"; // bom管理
import { spareGetForm } from "@/api/attachment/account.js"; // 备件台账
import { inspectGetForm } from "@/api/equipmentList/inspectionStand.js"; // 巡检标准
import { pointGetForm } from "@/api/equipmentList/spotCheckStand.js"; // 点检标准
import { attributeDictGetForm } from "@/api/workflows/dictionaryManage.js"; // 属性字典
import { maintainGetForm } from "@/api/equipmentList/maintainStand.js"; // 保养标准
import { workOrderGetForm } from "@/api/mochaItom/repairOrder.js"; // 工单管理
import { libCategoryGetForm } from "@/api/repository/categoryManage.js"; // 品类管理
import { requestInfoGetForm } from "@/api/mochaItom/serviceRequest.js"; // 服务请求
import { brandGetForm } from "@/api/repository/brandmanage.js"; //  品牌管理
// 已存在表单
const formList = JSON.parse(sessionStorage.getItem("formList")) || [];
export default async function getSingleFormId(subscript) {
  const formId = returnId(subscript);
  if (formId !== "") {
    return formId;
  }

  let getFormFn;
  let idKey;

  switch (subscript) {
    case 1587342779416342531:
      getFormFn = customerGetForm;
      idKey = 1587342779416342531;
      break;
    case 1638727171054682113:
      getFormFn = excItemGetForm;
      idKey = 1638727171054682113;
      break;
    case 1587321046532603906:
      getFormFn = partnerLinkmanGetForm;
      idKey = 1587321046532603906;
      break;
    case 1592076500560306178:
      getFormFn = projectListGetForm;
      idKey = 1592076500560306178;
      break;
    case 1619565125748015106:
      getFormFn = equipmentListGetForm;
      idKey = 1619565125748015106;
      break;
    case 1598948380864999426:
      getFormFn = bomManagementGetForm;
      idKey = 1598948380864999426;
      break;
    case 1592796580774334467:
      getFormFn = spareGetForm;
      idKey = 1592796580774334467;
      break;
    case 1624279538517327873:
      getFormFn = inspectGetForm;
      idKey = 1624279538517327873;
      break;
    case 1624284025340477442:
      getFormFn = pointGetForm;
      idKey = 1624284025340477442;
      break;
    case 1624284569366872065:
      getFormFn = maintainGetForm;
      idKey = 1624284569366872065;
      break;
    case 1610894472151977987:
      getFormFn = attributeDictGetForm;
      idKey = 1610894472151977987;
      break;
    case 1631095475858042881:
      getFormFn = workOrderGetForm;
      idKey = 1631095475858042881;
      break;
    case 1610096198719705091:
      getFormFn = libCategoryGetForm;
      idKey = 1610096198719705091;
      break;
    case 1630018112074330114:
      getFormFn = requestInfoGetForm;
      idKey = 1630018112074330114;
      break;
    case 1612256175009849347:
      getFormFn = brandGetForm;
      idKey = 1612256175009849347;
      break;
    // 其他情况
    default:
      return "";
  }

  const response = await getFormFn();
  formList.push({
    [idKey]: response.data.id,
    formName: response.data.formName,
  });

  await updateFormListInSessionStorage(formList);

  return response.data.id;
}

function returnId(subscript) {
  let isValue = "";
  for (var i = 0; i < formList.length; i++) {
    for (let key in formList[i]) {
      if (key == subscript) {
        isValue = formList[i][key];
      }
    }
  }
  return isValue;
}

async function updateFormListInSessionStorage(formList) {
  sessionStorage.setItem("formList", JSON.stringify(formList));
}
