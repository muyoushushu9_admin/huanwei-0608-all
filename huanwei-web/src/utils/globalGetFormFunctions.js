import { spareRequestCodeGetForm } from '@/api/attachment/account.js';
// 外部公用查备件申请表单的接口获取字段
export async function getSpareRequestCodeGetForm() {
    let tableJsonData = [];
    let tableJsonF = [];
    let formConf = [];
    let formId = '';

    try {
        const response = await spareRequestCodeGetForm();
        JSON.parse(response.data.defineJson).forEach((item, index) => {
            if (item.formRef === undefined) {
                let myItem = {
                    ...item,
                    visible: true,
                    key: index
                };
                // if (item.vModel != 'spareContact') {
                tableJsonData.push(myItem);
                tableJsonF.push(myItem);
                // }

            } else {
                if (item.vModel !== 'spareContactTool' && item.vModel !== 'spareContactMaterial') {
                    formConf.push(item);
                }
            }
        });
        formId = response.data.id;
        return { tableJsonData, tableJsonF, formConf, formId };
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}