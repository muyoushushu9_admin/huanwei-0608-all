import {
	config
} from './config'

import Vue from 'vue';
import store from '@/store'


/**
 * 获取按钮有没有权限显示
 * 
 * @param {Object} model   页面类型 
 * @param {Object} pageKey  页面标识
 * @param {Object} overData  额外参数为对象
 * 
 */
function authorityJudgment(model, pageKey, overData) {
	// let roleList = config.roleList;
	// let userRoles=store.state.user.roles[0]
	// roleList[roles].indexOf(roleKey);

	//设置为菜单权限控制类型  如果有某个左侧菜单的权限那么显示
	if (model == 'menu') {
		for (var i = 0; i < store.state.permission.addRoutes.length; i++) {
			var menu = store.state.permission.addRoutes[i];
			//如果当前页面隐藏就跳过
			if (menu.children) {
				for (var s = 0; s < menu.children.length; s++) {
					if (menu.children[s].path) {
						if (menu.children[s].path == pageKey) {
							return true
						}
					}
				}
			}
		}
		return false
	}

	//设置为按钮权限控制类型  通过权限配置页的权限字符进行显隐
	if (model == 'button') {
		//如果是admin就直接通过
		if (store.state.user.permissions[0] == "*:*:*") {
			return true;
		}

		let s = store.state.user.permissions.indexOf(pageKey)
		return s == -1 ? false : true
	}


	//如果自定义的根据不同key写不同逻辑
	if (model == 'custom') {
	}



}

export default {
	authorityJudgment
}