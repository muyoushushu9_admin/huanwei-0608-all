import screenfull from 'screenfull'; // 引入全屏显示
import store from '@/store/index.js'

export default {
	install(Vue) {
		//计算列表高度  为了让列表按规定样式  h为上部筛选栏高度   screenfull为是否全屏 
		Vue.prototype.calculateTableHeight = function (h, screenfull) {
			if (screenfull) { //如果是全屏的
				var vh = window.screen.height;
				var navHieght = 0; //导航栏固定高度为0
				var height = navHieght + h + 20
			} else {
				var vh = document.body.clientHeight;
				var navHieght = 126; //导航栏固定高度为136
				var height = navHieght + h
			}
			return {
				boxHeight: vh - height,
				tableHeight: vh - height - 40 - 60  //盒子内部padding 标题大小  
			}
		}
		//全屏
		Vue.prototype.screenfulls = function (element) {
			if (screenfull.isEnabled) {
				screenfull.request(element); // 元素全屏
			} else {
				this.$message({
					message: '你的浏览器不支持全屏',
					type: 'warning'
				})
			}
			let checkFull = () => {
				// 判断浏览器是否处于全屏状态 （需要考虑兼容问题）
				var isFull = document.mozFullScreen || document.fullScreen || document.webkitIsFullScreen ||
					document.webkitRequestFullScreen || document.mozRequestFullScreen || document
						.msFullscreenEnabled
				if (isFull === undefined) {
					isFull = false
				}
				return isFull;
			}

			window.onresize = function () {
				if (!checkFull()) {
					// 退出全屏后要执行的动作
					store.dispatch("app/setFullscreen", false);
				} else {
					store.dispatch("app/setFullscreen", true);
				}
			}
		}

	}
}
