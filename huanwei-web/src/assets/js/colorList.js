export const typeColor = ['#00C16E', '#F4754B', '#037EF3', '#FFC845', '#ccc'];
export const statusColor = ['#00C16E', '#F4754B', '#037EF3', '#FFC845', '#ccc'];
export const urgencyLevelTypeColor = ['#FFBA00', '#909399', '#FF4949'];//维修记录-紧急程度
export const faultLevelTypeColor = ['#FFBA00', '#909399', '#FF4949'];//维修记录-故障等级
export const statusTypeColor = ['#1677FF', '#FFBA00', '#D953EF', '#07C160', '#909399', '#FF4949'];//服务事件-事件状态
export const stateTypeColor = ['#1677FF', '#07C160', '#FFBA00', '#D953EF', '#395484', '#909399', '#FF4949', '#ccc'];//服务事件-事件状态
export const applystateTypeColor = ['#1677FF', '#FFBA00', '#D953EF', '#909399', '#FF4949', '#07C160'];//备件申请-状态