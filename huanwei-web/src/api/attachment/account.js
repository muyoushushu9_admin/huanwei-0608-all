import request from "@/utils/request";

// 客户管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
export function spareGetForm() {
  return request({
    url: '/dev/sparePartDetailList/getForm',
    method: 'get',
  })
}
/**
 * 表单查询  备件管理
 * @param 
 * @returns
 */
export function spareGetForms() {
  return request({
    url: '/dev/sparePartDetailList/spareDetail/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function spareQueryPageList(query) {
  return request({
    // url: '/dev/sparePartDetailList/queryPageList',
    url: '/dev/sparePartDetailList/queryPageListSame',//合并同类项
    method: 'get',
    params: query
  })
}
/**
 * 分页查询(设备列表下备件)
 * @param {条件} query
 * @returns
 */
export function spareQueryPageLists(query) {
  return request({
    url: '/dev/sparePartDetailList/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function spareAdd(data) {
  return request({
    url: '/dev/sparePartDetailList',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function spareEdit(data) {
  return request({
    url: '/dev/sparePartDetailList',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function spareDel(id) {
  return request({
    url: '/dev/sparePartDetailList/' + id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function spareDelbatch(id) {
  return request({
    url: '/dev/sparePartDetailList/batch/' + id,
    method: 'delete'
  })
}
/**
 * 设备列表下备件批量删除
 * @returns
 */
export function spareDelbatchs(query) {
  return request({
    url: '/dev/sparePartDetailList/spareDel',
    method: 'delete',
    params: query
  })
}

// 编辑表格使用
export function spareDetail(id) {
  return request({
    url: '/dev/sparePartDetailList/' + id,
    method: 'get'
  })
}


/**
 * 产品列表内 备件表单定义查询
 * @param 
 * @returns
 */
export function devSpareGetForm() {
  return request({
    url: '/lib/product/devSpare/getForm',
    method: 'get',
  })
}
/**
 * 产品列表内 备件查询
 * @param 
 * @returns
 */
export function devSpareQueryPageList(query) {
  return request({
    url: '/lib/product/devSpare/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 产品列表内 备件新增
 * @returns
 */
export function devSpareAdd(data) {
  return request({
    url: '/lib/product/devSpare',
    method: 'post',
    data: data
  })
}
/**
 * 产品列表内 备件修改
 * @returns
 */
export function devSpareEdit(data) {
  return request({
    url: '/lib/product/devSpare',
    method: 'put',
    data: data
  })
}
/**
 * 产品列表内 备件删除
 * @returns
 */
export function devSpareDel(id) {
  return request({
    url: '/lib/product/devSpare/' + id,
    method: 'delete'
  })
}
/**
 * 产品列表内 备件删除
 * @returns
 */
export function devSpareDels(id) {
  return request({
    url: '/lib/product/devSpare/batch/' + id,
    method: 'delete'
  })
}
/**
 * 产品列表内 备件详情
 * @returns
 */
export function devSpareDetail(id) {
  return request({
    url: '/lib/product/devSpare/' + id,
    method: 'get'
  })
}
/**
 * 产品列表内 文档管理/产品图片
 * @returns
 */
export function productListFileInfo(data) {
  return request({
    url: '/system/productListFileInfo',
    method: 'post',
    params: data
  })
}
/**
 * 产品列表内 文档管理/产品图片查询
 * @returns
 */
export function productListFileInfoList(query) {
  return request({
    url: '/system/productListFileInfo/list',
    method: 'get',
    params: query
  })
}
/**
 * 备件关联设备
 * @returns
 */
export function getDevices(query) {
  return request({
    url: '/dev/sparePartDetailList/getDevices',
    method: 'get',
    params: query
  })
}

/**
 * 备件台账文件
 * @returns
 */
export function fileInfo(data) {
  return request({
    url: '/system/devSpare/fileInfo',
    method: 'post',
    params: data
  })
}

/**
 * 备件台账文件查询
 * @returns
 */
export function fileInfoList(query) {
  return request({
    url: '/system/devSpare/fileInfo/list',
    method: 'get',
    params: query
  })
}

/**
 * 备件申请单列表
 * @returns
 */
export function spareRequestCodeList(query) {
  return request({
    url: '/system/spareRequestCode/queryPageList',
    method: 'get',
    params: query
  })
}

/**
 *  备件申请表单查询
 * @param 
 * @returns
 */
export function spareRequestCodeGetForm() {
  return request({
    url: '/system/spareRequestCode/getForm',
    method: 'get',
  })
}

/**
 * 备件申请添加数据
 * @returns
 */
export function spareRequestCodeAdd(data) {
  return request({
    url: '/system/spareRequestCode',
    method: 'post',
    data: data
  })
}



// 编辑表格使用
export function spareRequestDetail(id) {
  return request({
    url: '/system/spareRequestCode/' + id,
    method: 'get'
  })
}

// 编辑表格使用
export function spareRequestDetail1(id) {
  return request({
    url: '/system/spareRequestCode/' + id,
    method: 'get'
  })
}
/**
 * 删除
 * @returns
 */
export function spareRequestCodeDel(id, query) {
  return request({
    url: '/system/spareRequestCode/' + id,
    method: 'delete',
    params: query
  })
}

/**
 * 修改
 * @returns
 */
export function spareRequestCodeEdit(data) {
  return request({
    url: '/system/spareRequestCode',
    method: 'put',
    data: data
  })
}
/**
 * 产品下保存备件
 * @returns
 */
export function productSpareBo(data) {
  return request({
    url: '/lib/product/devSpare/productBomBo',
    method: 'post',
    data: data
  })
}
/**
 * 设备详情产品下保存备件
 * @returns
 */
export function productSpareBoEqu(data) {
  return request({
    url: '/system/bomManagement/productSpare',
    method: 'post',
    data: data
  })
}
/**
 * 手动合并bom
 * @returns
 */
export function combineSpare(data) {
  return request({
    url: '/dev/sparePartDetailList/updateSpareBo',
    method: 'post',
    data: data
  })
}
/**
 * 恢复合并bom
 * @returns
 */
export function recoverSpare(data) {
  return request({
    url: '/dev/sparePartDetailList/recoverSpare',
    method: 'post',
    data: data
  })
}
/**
 * 备件清单中左侧树
 * @returns
 */
export function getSpareTreeList(query) {
  return request({
    url: '/dev/sparePartDetailList/treeSelect',
    method: 'get',
    params: query
  })
}
/**
 * 拖拽更新备件树
 * @returns
 */
export function updateDeptTreeOrder(data) {
  return request({
    url: '/dev/sparePartDetailList/batch',
    method: 'post',
    data: data
  })
}
/**
 * 设备详情 备件 从bom导入
 * @returns
 */
export function bomSpareBoEqu(data) {
  return request({
    url: '/dev/sparePartDetailList/importBom',
    method: 'post',
    data: data
  })
}
/**
 * 修改备件申请单状态
 * @returns
 */
export function spareRequestCodeStatusChange(data) {
  return request({
    url: '/system/spareRequestCode/updateStatus',
    method: 'put',
    data: data
  })
}