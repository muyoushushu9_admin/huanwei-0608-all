import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid,enterpriseId) {
	const data = {
		username,
		password,
		code,
		uuid,
		enterpriseId
	}
	return request({
		url: '/login',
		headers: {
			isToken: false
		},
		method: 'post',
		data: data
	})
}
// 短信登录方法
export function smsLogin(phonenumber, smsCode,enterpriseId) {
	const data = {
		phonenumber,
		smsCode,
		enterpriseId
	}
	return request({
		url: '/smsLogin',
		headers: {
			isToken: false
		},
		method: 'post',
		data: data
	})
}

// 注册方法
export function register(data) {
	return request({
		url: '/register',
		headers: {
			isToken: false
		},
		method: 'post',
		data: data
	})
}

// 获取用户详细信息
export function getInfo() {
	return request({
		url: '/getInfo',
		method: 'get'
	})
}

// 退出方法
export function logout() {
	return request({
		url: '/logout',
		method: 'post'
	})
}

// 获取验证码
export function getCodeImg() {
	return request({
		url: '/captchaImage',
		headers: {
			isToken: false
		},
		method: 'get',
		timeout: 20000
	})
}

// 短信验证码
export function getCodeSms() {
	return request({
		url: '/captchaSms',
		headers: {
			isToken: false
		},
		method: 'get',
		timeout: 20000
	})
}


// 校验验证码（注册用）
export function validImageCaptchaCode(query) {
	return request({
		url: '/validImageCaptchaCode',
		method: 'get',
		params: query
	})
}

// 发送短信验证码（注册用）
export function captchaSms(query) {
	return request({
		url: '/captchaSms',
		method: 'get',
		params: query
	})
}
// 校验验证码（注册用）
export function validateSmsCaptchaCode(query) {
	return request({
		url: '/validateSmsCaptchaCode',
		method: 'get',
		params: query
	})
}


// 获取账号对应的公司列表
export function getAccountEnterpriseList(query) {
	return request({
		url: '/getAccountEnterpriseList',
		method: 'get',
		params: query
	})
}

//获取首页配置
export function getKanbanConfig(query) {
	return request({
		url: '/system/kanbanConfig/list',
		method: 'get',
		params: query
	})
}
//修改首页配置
export function putKanbanConfig(data) {
	return request({
		url: '/system/kanbanConfig',
		method: 'put',
		data: data
	})
}


//获取首页二维码
export function canPcQrCode(query) {
	return request({
		url: '/system/scan/pcQrCode',
		method: 'get',
		params: query
	})
}


//获取首页二维码状态
export function getSessionStatus(query) {
	return request({
		url: '/system/scan/getSessionStatus',
		method: 'get',
		params: query
	})
}

//获取登录状态
export function getloginStatus(query) {
	return request({
		url: '/system/scan/getloginStatus',
		method: 'get',
		params: query
	})
}



