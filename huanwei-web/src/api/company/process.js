// 注册的企业账号审核
import request from "@/utils/request";
/**
 *  待审核列表
 * @param {条件} query
 * @returns
 */
export function auditPageList(query) {
  return request({
    url: '/system/company/auditPageList',
    method: 'post',
    params: query
  })
}

/**
 *  审核
 * @param {参数} query
 * @returns
 */
export function registAduit(query) {
  return request({
    url: '/system/company/registAduit',
    method: 'post',
    params: query
  })
}
// 企业信息详情
export function companyDetails(companyId) {
  return request({
    url: '/system/company/' + companyId,
    method: 'get'
  })
}

// 企业信息详情
export function companyDetail() {
  return request({
    url: '/system/company',
    method: 'get'
  })
}

