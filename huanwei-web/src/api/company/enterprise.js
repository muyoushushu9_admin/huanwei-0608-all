// 企业列表
import request from "@/utils/request";
/**
 *  列表
 * @param {条件} query
 * @returns
 */
 export function companyPage(query) {
  return request({
    url: '/system/company/page',
    method: 'post',
    params: query
  })
}

// 修改企业信息
export function companyEdit(data) {
  return request({
    url: '/system/company/edit',
    method: 'put',
    params: data
  })
}
// 冻结 解冻
export function companyFrozen(data) {
  return request({
    url: '/system/company/frozen',
    method: 'put',
    params: data
  })
}




