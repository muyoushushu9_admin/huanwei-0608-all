// 工程师列表
import request from "@/utils/request";
/**
 * 表单查询
 * @param 
 * @returns
 */
export function engineerGetForm() {
    return request({
        url: '/system/user/engineer/getForm',
        method: 'get',
    })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function engineerqueryPageList(query) {
    return request({
        url: '/system/user/engineer/queryPageList',
        method: 'get',
        params: query
    })
}
/**
 * 新增
 * @returns
 */
export function engineerAdd(data) {
    return request({
        url: '/system/user/engineer',
        method: 'post',
        data: data
    })
}
/**
 * 修改
 * @returns
 */
export function engineerEdit(data) {
    return request({
        url: '/system/user/engineer',
        method: 'put',
        data: data
    })
}
/**
 * 批量删除
 * @returns
 */
export function engineerDelbatch(ids) {
    return request({
        url: '/system/user/engineer/batch/' + ids,
        method: 'delete'
    })
}
// 详情
export function engineerDetail(id) {
    return request({
        url: '/system/user/engineer/' + id,
        method: 'get'
    })
}
// 顶部统计
export function engineerStatistics() {
    return request({
        url: '/system/user/engineer/statistics',
        method: 'get'
    })
}