import request from "@/utils/request";

// 项目计划

/**
 * 表单查询
 * @param 
 * @returns
 */
export function workOrderGetForm() {
	return request({
		url: '/sysProject/plan/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function workOrderPageList(query) {
	return request({
		url: '/sysProject/plan/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function workOrderInfoAdd(data) {
	return request({
		url: '/sysProject/plan',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function workOrderInfoEdit(data) {
	return request({
		url: '/sysProject/plan',
		method: 'put',
		data: data
	})
}
/**
 * 批量删除
 * @returns
 */
export function workOrderDelbatch(id) {
	return request({
		url: '/sysProject/plan/batch/' + id,
		method: 'delete'
	})
}
// 详情
export function workOrderInfoDetail(id) {
	return request({
		url: '/sysProject/plan/' + id,
		method: 'get'
	})
}

// 工单详情——解决方案
// 详情
export function attachmentUpdate(data) {
	return request({
		url: '/sysProject/plan/attachmentUpdate',
		method: 'put',
		data: data
	})
}

// 工单详情 任务部分
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function workTaskInfoPageList(query) {
	return request({
		url: '/sysPoject/planTask/queryPageList',
		method: 'get',
		params: query
	})
}
// 新增
export function workOrderTaskInfoAdd(data) {
	return request({
		url: '/sysPoject/planTask',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
 export function workOrderTaskInfoEdit(data) {
	return request({
		url: '/sysPoject/planTask',
		method: 'put',
		data: data
	})
}/**
 * 批量删除
 * @returns
 */
export function workOrderTaskDelbatch(id) {
	return request({
		url: '/sysPoject/planTask/batch/' + id,
		method: 'delete'
	})
}
// 详情
export function workOrderTaskInfoDetail(id) {
	return request({
		url: '/sysPoject/planTask/' + id,
		method: 'get'
	})
}
// 前置任务选择
export function prevNextTaskOptions(data) {
	return request({
		url: '/sysPoject/planTask/prevNextTaskOptions',
		method: 'get',
		params: data
	})
}
// 前置后置任务列表
export function prevNextTaskList(query) {
	return request({
		url: '/sysPoject/planTask/prevNextTaskList',
		method: 'get',
		params: query
	})
}
// 任务流转日志
export function taskGetLog(query) {
	return request({
		url: '/sysPoject/planTask/getLog',
		method: 'get',
		params: query
	})
}
// 工单开始功能
export function workStart(data) {
	return request({
		url: '/sysProject/plan/start',
		method: 'put',
		params: data
	})
}
// 工单 暂停功能
export function workSuspend(data) {
	return request({
		url: '/sysProject/plan/suspend',
		method: 'put',
		params: data
	})
}
// 工单 取消
export function workCancel(data) {
	return request({
		url: '/sysProject/plan/cancel',
		method: 'put',
		params: data
	})
}
// 工单完成
export function workFinish(data) {
	return request({
		url: '/sysProject/plan/finish',
		method: 'put',
		params: data
	})
}
// 工单 分配
export function workAssign(data) {
	return request({
		url: '/sysProject/plan/assign',
		method: 'put',
		params: data
	})
}
// 工单 转派
export function workReassign(data) {
	return request({
		url: '/sysProject/plan/reassign',
		method: 'put',
		params: data
	})
}
// 工单 流转日志
export function workGetLog(data) {
	return request({
		url: '/sysProject/plan/getLog',
		method: 'get',
		params: data
	})
}
// 工单任务 巡，点，保养，维修标准项
export function getStandard(data) {
	return request({
		url: '/workOrder/taskInfo/getStandard',
		method: 'get',
		params: data
	})
}
