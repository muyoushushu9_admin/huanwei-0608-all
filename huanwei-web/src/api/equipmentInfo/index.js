import request from '@/utils/request'

// 查询特种设备信息列表
export function listEquipmentInfo(query) {
  return request({
    url: '/system/equipmentInfo/list',
    method: 'get',
    params: query
  })
}

// 查询特种设备信息详细
export function getEquipmentInfo(id) {
  return request({
    url: '/system/equipmentInfo/' + id,
    method: 'get'
  })
}

// 新增特种设备信息
export function addEquipmentInfo(data) {
  return request({
    url: '/system/equipmentInfo',
    method: 'post',
    data: data
  })
}

// 修改特种设备信息
export function updateEquipmentInfo(data) {
  return request({
    url: '/system/equipmentInfo',
    method: 'put',
    data: data
  })
}

// 删除特种设备信息
export function delEquipmentInfo(id) {
  return request({
    url: '/system/equipmentInfo/' + id,
    method: 'delete'
  })
}
