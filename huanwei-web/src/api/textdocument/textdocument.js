import request from "@/utils/request";

// 设备服务协议


/**
 * 表单查询
 * @param 
 * @returns
 * "/system/documentInfo/list": {
	  "get": {
		"summary": "查询系统文档信息列表",
		 "/system/documentInfo/export": {
	  "post": {
		"summary": "导出",
		/system/documentInfo/{id}": {
	  "get": {
		"summary": "获取系统文档信息详细信息",
		 "/system/documentInfo": {
	  "post": {
		"summary": "新增系统文档信息",
		/system/documentInfo/batch/{ids}": {
	  "delete": {
		"summary": "删除系统文档信息",
		/system/documentInfo/getForm": {
	  "get": {
		"summary": "获取表单定义",
		/system/documentInfo/importTemplate": {
	  "post": {
		"summary": "模板下载",

		/system/documentInfo/import": {
		"post": {
		"summary": "导入",
 */
export function documentGetForm() {   //查询列表数据
	return request({
		url: '/system/documentInfo/getForm/',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function documentPageList(query) {   //表格数据  
	return request({
		url: '/system/documentInfo/list/',
		method: 'get',
		params: query
	})
}
/**
 * 分页查询文档管理
 * @param {条件} query
 * @returns
 */
export function documentPageLists(query) {   //表格数据  
	return request({
		// url: '/system/documentInfo/list/',
		url: '/system/documentInfo/pageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function documentInfoAdd(data) {    //添加
	return request({
		// url: '/system/documentInfo',
		url: '/system/documentInfo/batch',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function documentInfoEdit(data) {  //修改
	return request({
		url: '/system/documentInfo',
		method: 'put',
		data: data
	})
}

export function documentIndel(id) {     //单个删除    
	return request({
		url: '/system/documentInfo/' + id,
		method: 'delete'
	})
}
/**
 * 批量删除
 * @returns
 */
export function documentDelbatch(ids) {    //批量删除
	return request({
		url: '/system/documentInfo/batch/' + ids,
		method: 'delete'
	})
}
// 详情
export function documentInfoDetail(id) {    //详情
	return request({
		url: '/system/documentInfo/' + id,
		method: 'get'
	})
}

/**
 * 动态表单品牌组件使用的品牌列表
 * @param {条件} query
 * @returns
 */
export function workOrderpage(query) {
	return request({
		url: '/system/product/ProductPage',
		method: 'get',
		params: query
	})
}

// 导出
// 导入模板下载 /system/documentInfo/importTemplate
// 动态表单数据导入 /system/documentInfo/import
