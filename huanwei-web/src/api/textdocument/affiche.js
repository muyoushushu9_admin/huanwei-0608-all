import request from "@/utils/request";

// 设备服务协议

/**
 * 表单查询
 * @param 
 * @returns
 *
 */
export function afficheGetForm() {   //查询列表数据
	return request({
		url: '/system/noticeInfo/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function affichelist(query) {   //表格数据  
	return request({
		url: '/system/noticeInfo/list/',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function afficheInfoAdd(data) {    //添加
	return request({
		url: '/system/noticeInfo',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function afficheInfoEdit(data) {  //修改
	return request({
		url: '/system/noticeInfo',
		method: 'put',
		data: data
	})
}

export function documentIndel(id) {     //单个删除    
  return request({
    url: '/system/noticeInfo/'+id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function afficheDelbatch(ids) {    //批量删除
	return request({
		url: '/system/noticeInfo/batch/'+ ids,
		method: 'delete'
	})
}
// 详情
export function afficheInfo(id) {    //详情
	return request({
		url: '/system/noticeInfo/' + id,
		method: 'get'
	})
}

/**
 * 动态表单品牌组件使用的品牌列表
 * @param {条件} query
 * @returns
 */

