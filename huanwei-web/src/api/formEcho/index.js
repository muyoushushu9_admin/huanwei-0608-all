// 动态表单部分接口
import request from "@/utils/request";
/**
 * 流水号
 * @param {条件} query
 * @returns
 */
export function getSerialNumber(query) {
  return request({
    url: '/system/test1/getSerialNumber',
    method: 'get',
    params: query
  })
}
// 省市区数据
export function regionTreeList(query) {
  return request({
    url: '/system/region/treeList',
    method: 'get',
    params: query
  })
}
// 下拉树 删除选项
export function formTreeSelectComponentData(id) {
  return request({
    url: '/system/formTreeSelectComponentData/' + id,
    // url: '/system/formTreeSelectComponentData/batch/' + id,
    method: 'delete',
  })
}



