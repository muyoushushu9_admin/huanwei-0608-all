// 资源管理-客户管理
import request from "@/utils/request";
/**
 * 左侧树查询
 * @param 
 * @returns
 */
export function clientTree() {
    return request({
        url: '/system/client/queryCompanyList',
        method: 'get',
    })
}
/**
 * 右侧的表格查询
 * @param 
 * @returns
 */
export function clientList(query) {
    return request({
        url: '/system/client/queryPageList',
        method: 'get',
        params: query
    })
}
/**
 * 新增客户
 * @returns
 */
export function clientAdd(data) {
    return request({
        url: '/system/client',
        method: 'post',
        data: data
    })
}
/**
 * 修改客户
 * @returns
 */
export function clientEdit(data) {
    return request({
        url: '/system/client',
        method: 'put',
        data: data
    })
}
/**
 * 删除客户
 * @returns
 */
export function clientDel(id) {
    return request({
        url: '/system/client/' + id,
        method: 'delete'
    })
}
/**
 * 批量删除客户
 * @returns
 */
export function clientDelbatch(id) {
    return request({
        url: '/system/client/batch/' + id,
        method: 'delete'
    })
}
// 服务商班组部分
/**
 * 班组列表表格查询
 * @param 
 * @returns
 */
export function TeamGroupListAllClient(query) {
    return request({
        url: '/system/group/pageList',
        method: 'get',
        params: query
    })
}
/**
 * 新增服务商班组
 * @returns
 */
export function clientAddTeamGroup(data) {
    return request({
        url: '/system/group',
        method: 'post',
        data: data
    })
}
/**
 * 修改服务商班组
 * @returns
 */
export function clientEditTeamGroup(data) {
    return request({
        url: '/system/group',
        method: 'put',
        data: data
    })
}
/**
 * 删除服务商班组
 * @returns
 */
export function clientDelTeamGroup(id) {
    return request({
        url: '/system/group/' + id,
        method: 'delete'
    })
}