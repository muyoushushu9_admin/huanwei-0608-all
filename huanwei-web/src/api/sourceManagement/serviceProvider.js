// 资源管理-服务商管理
import request from "@/utils/request";
/**
 * 左侧树查询
 * @param 
 * @returns
 */
export function serviceProviderTree() {
    return request({
        url: '/system/service/queryCompanyList',
        method: 'get',
    })
}
/**
 * 右侧的表格查询
 * @param 
 * @returns
 */
export function serviceProviderList(query) {
    return request({
        url: '/system/service/queryPageList',
        method: 'get',
        params: query
    })
}
/**
 * 新增服务商
 * @returns
 */
export function serviceProviderAdd(data) {
    return request({
        url: '/system/service',
        method: 'post',
        data: data
    })
}
/**
 * 修改服务商
 * @returns
 */
export function serviceProviderEdit(data) {
    return request({
        url: '/system/service',
        method: 'put',
        data: data
    })
}
/**
 * 批量修改服务商
 * @returns
 */
export function serviceProviderEditBatch(data) {
    return request({
        url: '/system/service/batch',
        method: 'post',
        data: data
    })
}
/**
 * 删除服务商
 * @returns
 */
export function serviceProviderDel(id) {
    return request({
        url: '/system/service/' + id,
        method: 'delete'
    })
}
/**
 * 批量删除服务商
 * @returns
 */
export function serviceProviderDelbatch(id) {
    return request({
        url: '/system/service/batch/' + id,
        method: 'delete'
    })
}
// 服务商班组部分
/**
 * 班组列表表格查询
 * @param 
 * @returns
 */
export function TeamGroupListAll(query) {
    return request({
        url: '/system/group/pageList',
        method: 'get',
        params: query
    })
}
/**
 * 新增服务商班组
 * @returns
 */
export function serviceProviderAddTeamGroup(data) {
    return request({
        url: '/system/group',
        method: 'post',
        data: data
    })
}
/**
 * 修改服务商班组
 * @returns
 */
export function serviceProviderEditTeamGroup(data) {
    return request({
        url: '/system/group',
        method: 'put',
        data: data
    })
}
/**
 * 删除服务商班组
 * @returns
 */
export function serviceProviderDelTeamGroup(id) {
    return request({
        url: '/system/group/' + id,
        method: 'delete'
    })
}