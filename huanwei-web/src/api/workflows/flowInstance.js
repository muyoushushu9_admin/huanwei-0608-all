import request from "@/utils/request";


/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function flowInstancePage(query) {
  return request({
    url: '/system/enterpriseWorkflow/flowInstancePage',
    method: 'get',
    params: query
  })
}

/**
 * 任务列表查询
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowTaskPage(query) {
  return request({
    url: '/system/enterpriseWorkflow/taskPage',
    method: 'get',
    params: query
  })
}
/**
 * 任务列表 审核操作
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowAudit(query) {
  return request({
    url: '/system/enterpriseWorkflow/audit',
    method: 'post',
    params: query
  })
}
/**
 * 任务列表 撤回
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowCallback(query) {
  return request({
    url: '/system/enterpriseWorkflow/callback',
    method: 'post',
    params: query
  })
}
/**
 * 任务列表  取消流程
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowCancel(query) {
  return request({
    url: '/system/enterpriseWorkflow/cancel',
    method: 'post',
    params: query
  })
}
/**
 * 任务列表 转交
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowTransmit(query) {
  return request({
    url: '/system/enterpriseWorkflow/transmit',
    method: 'post',
    params: query
  })
}
/**
 * 审核列表查询
 * @param {条件} query
 * @returns
 */
 export function WorkflowMyTaskPage(query) {
  return request({
    url: '/system/enterpriseWorkflow/myTaskPage',
    method: 'get',
    params: query
  })
}
/**
 * 审核列表详情  表单数据
 * @param {条件} query
 * @returns
 */
 export function WorkflowFormDataDetail(query) {
  return request({
    url: '/system/enterpriseWorkflow/formDataDetail',
    method: 'get',
    params: query
  })
}

/**
 * 审核日志
 * @param {条件} query
 * @returns
 */
 export function WorkflowFlowInstanceLog(query) {
  return request({
    url: '/system/enterpriseWorkflow/flowInstanceLog',
    method: 'get',
    params: query
  })
}


