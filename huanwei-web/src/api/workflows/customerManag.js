import request from "@/utils/request";

// 客户管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
export function customerGetForm() {
  return request({
    url: '/system/customer/getForm',
    method: 'get',
  })
}
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function customerPageList(query) {
  return request({
    url: '/system/customer/queryPageList',
    method: 'get',
    params: query
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */

export function queryPartnerList(query) {
  return request({
    url: '/system/customer/queryPartnerList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function customerAdd(data) {
  return request({
    url: '/system/customer',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function customerEdit(data) {
  return request({
    url: '/system/customer',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function customerDel(id) {
  return request({
    url: '/system/customer/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function customerDetail(id) {
  return request({
    url: '/system/customer/' + id,
    method: 'get'
  })
}
// 顶部统计
export function customerStatistics() {
  return request({
    url: '/system/customer/statistics',
    method: 'get'
  })
}
/**
 * 删除
 * @returns
 */
export function customerPLDel(id) {
  return request({
    url: '/system/customer/batch/' + id,
    method: 'delete'
  })
}


// 客户服务历程
export function queryListByMonth(query) {
  return request({
    url: '/system/customerHistory/queryListByMonth',
    method: 'get',
    params: query
  })
}



