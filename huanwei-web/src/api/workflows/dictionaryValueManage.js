import request from "@/utils/request";

// 客户管理api
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function attributeDictPages(query) {
    return request({
        url: '/lib/attributeDictValue/page',
        method: 'get',
        params: query
    })
}
/**
 * 新增
 * @returns
 */
export function attributeDictAdds(data) {
    return request({
        url: '/lib/attributeDictValue/add',
        method: 'post',
        data: data
    })
}
/**
 * 修改
 * @returns
 */
export function attributeDictEdit(data) {
    return request({
        url: '/lib/attributeDictValue/edit',
        method: 'put',
        data: data
    })
}
/**
 * 修改(批量修改品类品牌)
 * @returns
 */
export function attributeDictEditBatch(data) {
    return request({
        url: '/lib/attributeDictValue/batch/edit',
        method: 'put',
        data: data
    })
}
/**
 * 删除
 * @returns
 */
export function attributeDictDel(id) {
    return request({
        url: '/lib/attributeDictValue/' + id,
        method: 'delete'
    })
}
/**
 * 批量删除
 * @returns
 */
export function attributeDictDelbatch(id) {
    return request({
        url: '/lib/attributeDictValue/batch/' + id,
        method: 'delete'
    })
}
// 编辑表格使用
export function attributeDictDelTail(id) {
    return request({
        url: '/lib/attributeDictValue/' + id,
        method: 'get'
    })
}




/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function formTreeSelectComponentDataPage(query) {
    return request({
        url: '/system/formTreeSelectComponentData/page',
        method: 'get',
        params: query
    })
}
/**
 * 新增
 * @returns
 */
export function formTreeSelectComponentDataAdd(data) {
    return request({
        url: '/system/formTreeSelectComponentData/add',
        method: 'post',
        data: data
    })
}
/**
 * 修改
 * @returns
 */
export function formTreeSelectComponentDataEdit(data) {
    return request({
        url: '/system/formTreeSelectComponentData/edit',
        method: 'put',
        data: data
    })
}
/**
 * 删除
 * @returns
 */
export function formTreeSelectComponentDataDele(id) {
    return request({
        url: '/system/formTreeSelectComponentData/' + id,
        method: 'delete'
    })
}
/**
 * 获取表单定义
 * @param {条件} query
 * @returns
 */
export function attributeDictGetForm() {
    return request({
        url: '/lib/attributeDictValue/getForm',
        method: 'get',
    })
}
/**
 * 模板可选的字典列表
 */
export function templateOptionDictList(query) {
    return request({
        url: '/lib/attributeDictValue/templateOptionDictList',
        method: 'get',
        params: query
    })
}


// 属性字典能否编辑
export function quoteEdit(id) {
    return request({
        url: '/system/dict/data/quote/' + id,
        method: 'get'
    })
}
