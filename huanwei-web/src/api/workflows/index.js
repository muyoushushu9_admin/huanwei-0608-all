import request from "@/utils/request";
/**
 * 动态表单左侧树结构查询（原先是post 改为了get）
 * @returns
 */
export function enterpriseFormFormModule(data) {
  return request({
    url: '/system/enterpriseForm/formModule',
    method: 'get',
    params: data
  })
}
/**
 * 动态表单列表新增
 * @returns
 */
export function enterpriseFormFormAdd(data) {
  return request({
    url: '/system/enterpriseForm/add',
    method: 'post',
    data: data
  })
}
/**
 * 动态表单列表修改
 * @returns
 */
 export function enterpriseFormEdit(data) {
  return request({
    url: '/system/enterpriseForm/edit',
    method: 'put',
    data: data
  })
}
/**
 * 动态表单分页查询
 * @param {条件} query
 * @returns
 */
 export function enterpriseFormPage(query) {
  return request({
    url: '/system/enterpriseForm/page',
    method: 'get',
    params: query
  })
}
/**
 * 工作流新增
 * @returns
 */
 export function enterpriseWorkflowAdd(data) {
  return request({
    url: '/system/enterpriseWorkflow/add',
    method: 'post',
    data: data
  })
}
/**
 * 工作流表单分页查询
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowPage(query) {
  return request({
    url: '/system/enterpriseWorkflow/page',
    method: 'get',
    params: query
  })
}
/**
 * 工作流删除
 * @param {条件} query
 * @returns
 */
 export function enterpriseWorkflowDelete(id) {
  return request({
    url: '/system/enterpriseWorkflow/batch/'+id,
    method: 'delete'
  })
}
/**
 * 工作流编辑 —— 获取动态表单字段
 * @param {条件} query
 * @returns
 */
 export function enterpriseFormGetByFormCode(query) {
  return request({
    url: '/system/enterpriseForm/getByFormCode',
    method: 'get',
    params: query
  })
}

/**
 * 工作流配置内容查询
 * @returns
 */
 export function enterpriseWorkflow(id) {
  return request({
    url: '/system/enterpriseWorkflow/' + id,
    method: 'get',
  })
}
/**
 * 工作流编辑
 * @returns
 */
 export function enterpriseWorkflowEdit(data) {
  return request({
    url: '/system/enterpriseWorkflow/edit',
    method: 'put',
    data: data
  })
}
/**
 * 工作流节点获取最新版本
 * @returns
 */
 export function enterpriseWorkflowGetByCod(data) {
  return request({
    url: '/system/enterpriseWorkflow/getByCode',
    method: 'get',
    params: data
  })
}





