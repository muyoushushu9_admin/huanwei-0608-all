import request from "@/utils/request";

/**
 * 表单查询
 * @param 
 * @returns
 */
 export function testFormGetForm() {
  return request({
    url: '/system/test1/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function testPage(query) {
  return request({
    url: '/system/test1/page',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function testAdd(data) {
  return request({
    url: '/system/test1/add',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function testEdit(data) {
  return request({
    url: '/system/test1/edit',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function testDel(id) {
  return request({
    url: '/system/test1/'+id,
    method: 'delete'
  })
}

