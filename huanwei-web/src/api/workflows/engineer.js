import request from "@/utils/request";

// 自有工程师管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
export function engineerGetForm() {
    return request({
        url: 'system/enterpriseForm',
        method: 'get',
    })
}
// /system/enterpriseForm/page?pageNum=1&pageSize=10&businessCode=1598938210290143233