import request from "@/utils/request";
// 供应商管理api

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function invitePageList(query) {
  return request({
    url: '/system/invite/list',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function inviteAdd(data) {
  return request({
    url: '/system/invite',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function inviteEdit(data) {
  return request({
    url: '/system/invite',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function inviteDel(id) {
  return request({
    url: '/system/invite/'+id,
    method: 'delete'
  })
}
/**
 * 详情
 * @returns
 */
export function inviteDetail(id) {
  return request({
    url: '/system/invite/' + id,
    method: 'get'
  })
}

