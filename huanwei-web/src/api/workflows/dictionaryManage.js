import request from "@/utils/request";

// 客户管理api
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function attributeDictPage(query) {
  return request({
    url: '/lib/attributeDict/page',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function attributeDictAdd(data) {
  return request({
    url: '/lib/attributeDict/add',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function attributeDictEdit(data) {
  return request({
    url: '/lib/attributeDict/edit',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function attributeDictDel(id) {
  return request({
    url: '/lib/attributeDict/' + id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function attributeDictDelbatch(id) {
  return request({
    url: '/lib/attributeDict/batch/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function attributeDictDelTail(id) {
  return request({
    url: '/lib/attributeDict/' + id,
    method: 'get'
  })
}




/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function formTreeSelectComponentDataPage(query) {
  return request({
    url: '/system/formTreeSelectComponentData/page',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function formTreeSelectComponentDataAdd(data) {
  return request({
    url: '/system/formTreeSelectComponentData/add',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function formTreeSelectComponentDataEdit(data) {
  return request({
    url: '/system/formTreeSelectComponentData/edit',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function formTreeSelectComponentDataDele(id) {
  return request({
    url: '/system/formTreeSelectComponentData/' + id,
    method: 'delete'
  })
}
/**
 * 获取表单定义
 * @param {条件} query
 * @returns
 */
export function attributeDictGetForm() {
  return request({
    url: '/lib/attributeDict/getForm',
    method: 'get',
  })
}
/**
 * 模板可选的字典列表
 */
export function templateOptionDictList(query) {
  return request({
    url: '/lib/attributeDict/templateOptionDictList',
    method: 'get',
    params: query
  })
}
/**
 * 模板库品类品牌配置可选的字典列表
 */
export function templateOptionDictLists(query) {
  return request({
    url: '/lib/attributeDict/getConfigureProductAttributes',
    method: 'get',
    params: query
  })
}


// 属性字典能否编辑
export function quoteEdit(id) {
  return request({
    url: '/system/dict/data/quote/' + id,
    method: 'get'
  })
}

// 属性字典值查看（点击产品字典管理列表中字典名称显示字典值）
export function checkAttributeDictValue(query) {
  return request({
    url: '/lib/attributeDictValue/list/',
    method: 'get',
    params: query
  })
}


/**
 * 字典值获取表单定义
 * @param {条件} query
 * @returns
 */
export function attributeDictValueGetForm() {
  return request({
    url: '/lib/attributeDictValue/getForm',
    method: 'get',
  })
}