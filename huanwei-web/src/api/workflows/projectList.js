import request from "@/utils/request";

// 客户管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
export function projectListGetForm() {
	return request({
		url: '/system/projectList/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function projectListPageList(query) {
	return request({
		url: '/system/projectList/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 分页查询 选择表格内  
 * @param {条件} query
 * @returns
 */
export function projectListPageListGeneral(query) {
	return request({
		url: '/system/projectList/queryPageListGeneral',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function projectListAdd(data) {
	return request({
		url: '/system/projectList',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function projectListEdit(data) {
	return request({
		url: '/system/projectList',
		method: 'put',
		data: data
	})
}
/**
 * 删除
 * @returns
 */
export function projectListDel(id) {
	return request({
		url: '/system/projectList/' + id,
		method: 'delete'
	})
}
/**
 * 批量删除
 * @returns
 */
export function projectListDelbatch(id) {
	return request({
		url: '/system/projectList/batch/' + id,
		method: 'delete'
	})
}
// 详情
export function projectListDetail(id) {
	return request({
		url: '/system/projectList/' + id,
		method: 'get'
	})
}

// 顶部统计
export function projectListStatistics() {
	return request({
		url: '/system/projectList/statistics',
		method: 'get'
	})
}


// 复制项目
export function copySysProjectList(data) {
	return request({
		url: '/system/projectList/copySysProjectList',
		method: 'post',
		params: data
	})
}

// 归档项目
export function archiveProjectList(data) {
	return request({
		url: '/system/projectList/archiveProjectList',
		method: 'put',
		params: data
	})
}
// 我方项目成员查询
export function projectListOur(id) {
	return request({
		url: '/system/projectList/our/' + id,
		method: 'get'
	})
}
// 合作方项目成员查询
export function projectListPartner(id) {
	return request({
		url: '/system/projectList/customerProjectMember/' + id,
		method: 'get'
	})
}
// 合作方项目成员修改
export function editCustomerProjectMember(data) {
	return request({
		url: '/system/projectList/editCustomerProjectMember',
		method: 'put',
		params: data
	})
}
// 合作方项目成员删除
export function deleteCustomerProjectMember(data) {
	return request({
		url: '/system/projectList/deleteCustomerProjectMember',
		method: 'delete',
		params: data
	})
}
// 修改我方项目成员
export function editOurProjectMember(data) {
	return request({
		url: '/system/projectList/editOurProjectMember',
		method: 'put',
		params: data
	})
}
// 删除我方项目成员
export function editOurProjectMemberDelete(data) {
	return request({
		url: '/system/projectList/deleteOurProjectMember',
		method: 'delete',
		params: data
	})
}
// 项目历程
export function queryListByMonth(query) {
	return request({
		url: '/system/serviceHistory/queryListByMonth',
		method: 'get',
		params: query
	})
}
