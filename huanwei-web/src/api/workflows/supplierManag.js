import request from "@/utils/request";

// 供应商管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
 export function supplierGetForm() {
  return request({
    url: '/system/supplier/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function supplierQueryPageList(query) {
  return request({
    url: '/system/supplier/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function supplierAdd(data) {
  return request({
    url: '/system/supplier',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function supplierEdit(data) {
  return request({
    url: '/system/supplier',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function supplierDelbatch(id) {
  return request({
    url: '/system/supplier/batch/'+id,
    method: 'delete'
  })
}
// 详情
export function supplierDetail(id) {
  return request({
    url: '/system/supplier/' + id,
    method: 'get'
  })
}

// 顶部统计
export function supplierStatistics() {
  return request({
    url: '/system/supplier/statistics',
    method: 'get'
  })
}