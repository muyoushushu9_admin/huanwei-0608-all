import request from "@/utils/request";

// 筛选条件和动态列api

/**
 * 查询系统企业用户筛选字段列表
 * @param 
 * @returns
 */
export function companyUserFilterFieldList(query) {
	return request({
		url: '/system/companyUserFilterField/myList',
		method: 'get',
		params: query
	})
}

/**
 * 批量修改系统企业用户筛选字段列表
 * @returns
 */
export function companyUserFilterFieldBatchEdit(data) {
	return request({
		url: '/system/companyUserFilterField/batchEdit',
		method: 'put',
		data: data
	})
}

/**
 * 查询系统企业用户列字段列表
 * @param 
 * @returns
 */
export function companyUserListFieldList(query) {
	return request({
		url: '/system/companyUserListField/myList',
		method: 'get',
		params: query
	})
}

/**
 * 批量修改系统企业用户列字段列表
 * @returns
 */
export function companyUserListFieldBatchEdit(data) {
	return request({
		url: '/system/companyUserListField/batchEdit',
		method: 'put',
		data: data
	})
}

/**
 * 查询系统企业用户列字段排序列表（自己的）
 * @returns
 */
export function companyUserFilterFieldSortMyList(query) {
	return request({
		url: '/system/companyUserFilterFieldSort/myList',
		method: 'get',
		params: query
	})
}

/**
 * 批量修改系统企业用户列字段排序
 * @returns
 */
export function companyUserFilterFieldSortBatchEdit(data) {
	return request({
		url: '/system/companyUserFilterFieldSort/batchEdit',
		method: 'put',
		data: data
	})
}

/**
 * 设置redis缓存的系统表单数据
 * @returns
 */
export function addFormCacheData(data) {
	return request({
		url: '/system/formCache/addFormCacheData',
		method: 'post',
		data: data
	})
}

/**
 * 获取redis缓存的系统表单数据
 * @returns
 */
export function queryFormCacheData(query) {
	return request({
		url: '/system/formCache/queryFormCacheData',
		method: 'get',
		params: query
	})
}
/**
 * 客户管理纳税人识别号唯一性校验
 * @returns
 */
export function customerDuplicateCheck(query) {
	return request({
		url: `/system/customer/duplicateCheck`,
		method: 'get',
		params: query
	})
}