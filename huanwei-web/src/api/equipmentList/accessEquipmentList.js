
// 附属设备
import request from "@/utils/request";

/**
 * 表单查询
 * @param 
 * @returns
 */
 export function accessEquipmentListGetForm() {
  return request({
    url: '/system/accessEquipmentList/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function accessEquipmentListQueryPageList(query) {
  return request({
    url: '/system/accessEquipmentList/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function accessCustomerAdd(data) {
  return request({
    url: '/system/accessEquipmentList',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function accessCustomerEdit(data) {
  return request({
    url: '/system/accessEquipmentList',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function accessCustomerDel(id) {
  return request({
    url: '/system/accessEquipmentList/batch/'+id,
    method: 'delete'
  })
}
// 编辑表格使用
export function accessCustomerDetail(id) {
  return request({
    url: '/system/accessEquipmentList/' + id,
    method: 'get'
  })
}