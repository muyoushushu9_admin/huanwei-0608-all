import request from "@/utils/request";

//  设备 巡检标准

/**
 * 表单查询
 * @param 
 * @returns
 */
export function inspectGetForm() {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function inspectPageList(query) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function inspectDetailPageList(query) {
  return request({
    url: '/system/orderTaskItemInfo/queryEquipmentPageList',
    method: 'get',
    params: query
  })
}

/**
 * 新增
 * @returns
 */
export function inspectAdd(data) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function inspectEdit(data) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function inspectDel(id) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/' + id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function inspectDelbatch(id) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/batch/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function inspectDetail(id) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/' + id,
    method: 'get'
  })
}

// 编辑表格使用
export function taskDetail(id) {
  return request({
    url: '/system/orderTaskItemInfo/' + id,
    method: 'get'
  })
}
// 查询设备下的bom对应的标准项
export function getPatrolInspection(data) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/getPatrolInspectionTaskItemInfoByDeviceId',
    method: 'get',
    params: data
  })
}


// 设备下详情下的点检 表格的 新增和修改

export function getinspectAdd(data) {
  return request({
    url: '/system/devPointInspectionTaskInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function getinspectEdit(data) {
  return request({
    url: '/system/devPointInspectionTaskInfo',
    method: 'put',
    data: data
  })
}
// 查询设备下的bom对应的标准项  点检 新增标准表格 查询现有标准
export function getSpotcheck(data) {
  return request({
    url: '/system/devPointInspectionTaskInfo/getPointInspectionTaskItemInfoByDeviceId',
    method: 'get',
    params: data
  })
}

/**
 * 切换状态 巡检
 * @param {条件} query
 * @returns
 */
export function changeDataPlanStatus(query) {
  return request({
    url: '/system/devPatrolInspectionTaskInfo/changeStatus',
    method: 'put',
    params: query
  })
}
