
// 模板库产品列表下 bom 管理
import request from "@/utils/request";

/**
 * 表单查询
 * @param 
 * @returns
 */
export function bomManagementGetForm() {
  return request({
    url: '/system/productListBomInfo/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function bomManagementList(query) {
  return request({
    url: '/system/productListBomInfo/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function bomManagementAdd(data) {
  return request({
    url: '/system/productListBomInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function bomManagementEdit(data) {
  return request({
    url: '/system/productListBomInfo',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function bomManagementDel(id) {
  return request({
    url: '/system/productListBomInfo/batch/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function bomManagementDetail(id) {
  return request({
    url: '/system/productListBomInfo/' + id,
    method: 'get'
  })
}
// 更新层级
export function bomManagementUpdateLevel() {
  return request({
    url: '/system/productListBomInfo/updateLevel',
    method: 'put'
  })
}
// 获取Bom层级下拉树列表
export function bomManagementTreeSelect(query) {
  return request({
    url: '/system/productListBomInfo/treeSelect',
    method: 'get',
    params: query
  })
}
/**
 * 拖拽更新bom树
 * @returns
 */
export function updateDeptTreeOrder(data) {
  return request({
    url: '/system/productListBomInfo/batch',
    method: 'post',
    data: data
  })
}
/**
 * 产品保存bom
 * @returns
 */
export function productBomBo(data) {
  return request({
    url: '/system/productListBomInfo/productBomBo',
    method: 'post',
    data: data
  })
}
