import request from "@/utils/request";

// 设备服务协议

/**
 * 表单查询
 * @param 
 * @returns
 */
export function workOrderGetForm() {   //查询列表数据
	return request({
		url: '/system/serviceContract/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function workOrderPageList(query) {   //表格数据  
	return request({
		url: '/system/serviceContract/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function workOrderInfoAdd(data) {    //添加
	return request({
		url: '/system/serviceContract/',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function workOrderInfoEdit(data) {  //修改
	return request({
		url: '/system/serviceContract',
		method: 'put',
		data: data
	})
}

export function workOrderIndel(id) {     //单个删除    
  return request({
    url: '/system/serviceContract/'+id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function workOrderDelbatch(ids) {    //批量删除
	return request({
		url: '/system/serviceContract/batch/'+ ids,
		method: 'delete'
	})
}
// 详情
export function workOrderInfoDetail(id) {    //详情
	return request({
		url: '/system/serviceContract/' + id,
		method: 'get'
	})
}

/**
 * 动态表单品牌组件使用的品牌列表
 * @param {条件} query
 * @returns
 */
export function workOrderpage(query) {
	return request({
		url: '/system/product/ProductPage',
		method: 'get',
		params: query
	})
}

// 导出 
// 导入模板下载 /system/serviceContract/importTemplate
// 动态表单数据导入  /system/serviceContract/import
