import request from "@/utils/request";

// 设备 点检标准

/**
 * 表单查询
 * @param 
 * @returns
 */
export function pointGetForm() {
  return request({
    url: '/system/devPointInspectionTaskInfo/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function pointPageList(query) {
  return request({
    url: '/system/devPointInspectionTaskInfo/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function pointAdd(data) {
  return request({
    url: '/system/devPointInspectionTaskInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function pointEdit(data) {
  return request({
    url: '/system/devPointInspectionTaskInfo',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function pointDel(id) {
  return request({
    url: '/system/devPointInspectionTaskInfo/' + id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function pointDelbatch(id) {
  return request({
    url: '/system/devPointInspectionTaskInfo/batch/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function pointDetail(id) {
  return request({
    url: '/system/companyUserFilterFieldSort/myList/',
    method: 'get',
    params: id
  })
}

// 设备点检详情
export function pointDetails(id) {
  return request({
    url: '/system/devPointInspectionTaskInfo/' + id,
    method: 'get'
  })
}

/**
 * 切换状态 点检
 * @param {条件} query
 * @returns
 */
export function changeDataPlanStatusPoint(query) {
  return request({
    url: '/system/devPointInspectionTaskInfo/changeStatus',
    method: 'put',
    params: query
  })
}


