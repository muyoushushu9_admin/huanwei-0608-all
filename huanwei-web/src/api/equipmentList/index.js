import request from "@/utils/request";


/**
 * 表单查询
 * @param 
 * @returns
 */
export function equipmentListGetForm() {
  return request({
    url: '/system/equipmentList/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function equipmentListQueryPageList(query) {
  return request({
    url: '/system/equipmentList/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function customerAdd(data) {
  return request({
    url: '/system/equipmentList',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function customerEdit(data) {
  return request({
    url: '/system/equipmentList',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function customerDel(id) {
  return request({
    url: '/system/equipmentList/batch/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function customerDetail(id) {
  return request({
    url: '/system/equipmentList/' + id,
    method: 'get'
  })
}
// 顶部统计
export function customerStatistics() {
  return request({
    url: '/system/customer/statistics',
    method: 'get'
  })
}

// 左侧类别树查询(以设备)
export function queryEquipmentCategoryTree() {
  return request({
    url: '/system/equipmentCategory/queryEquipmentCategoryTree',
    method: 'get'
  })
}
/**
 * 左侧类别树查询(以品类)
 * @param 
 * @returns
 */
export function queryCategoryTree() {
  return request({
    url: '/system/equipmentCategory/queryCategoryTree',
    method: 'get',
  })
}
//查询设备配置保存的专有属性
export function getexclusiveAttributes(id) {
  return request({
    url: `system/templateEnterpriseConfig/queryEnterpriseConfigList?enterpriseId=${id}`,
    method: 'get'
  })
}
//设备和设备列表详情中的bom列表编辑时更换产品时是否删除原有bom和备件
export function changeDelBom(data) {
  return request({
    url: `/system/equipmentList/bom`,
    method: 'delete',
    data: data
  })
}


