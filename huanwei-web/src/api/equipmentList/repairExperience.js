import request from "@/utils/request";

// 设备 维修经验

/**
 * 表单查询
 * @param 
 * @returns
 */
export function fixExpGetForm() {
  return request({
    url: '/system/devFixExpInfo/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function devFixPageList(query) {
  return request({
    url: '/system/devFixExpInfo/getByDeviceId',
    method: 'get',
    params: query
  })
}
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function devFixPageList1(query) {
  return request({
    url: '/system/devFixExpInfo/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function maintainAdd(data) {
  return request({
    url: '/system/devFixExpInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function maintainEdit(data) {
  return request({
    url: '/system/devFixExpInfo',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function maintainDel(id) {
  return request({
    url: '/system/devFixExpInfo/' + id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function maintainDelbatch(id) {
  return request({
    url: '/system/devFixExpInfo/batch/' + id,
    method: 'delete'
  })
}
// 编辑表格使用
export function maintainDetail(id) {
  return request({
    url: '/system/devFixExpInfo/' + id,
    method: 'get'
  })
}
// 保养标准 列表查询
export function taskItemInfo(query) {
  return request({
    url: '/system/taskItemInfo/list',
    method: 'get',
    params: query
  })
}

/**
 * 新增
 * @returns
 */
export function taskItemInfoAdd(data) {
  return request({
    url: '/system/taskItemInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function taskItemInfoEdit(data) {
  return request({
    url: '/system/taskItemInfo',
    method: 'put',
    data: data
  })
}
// 编辑表格使用
export function taskItemInfoDetail(id) {
  return request({
    url: '/system/taskItemInfo/' + id,
    method: 'get'
  })
}
/**
 * 删除
 * @returns
 */
export function taskItemInfoDel(id) {
  return request({
    url: '/system/taskItemInfo/' + id,
    method: 'delete'
  })
}