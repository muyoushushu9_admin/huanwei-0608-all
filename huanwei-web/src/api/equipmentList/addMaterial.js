import request from "@/utils/request";
// 物料清单

/**
 * 分页查询(物料/工具)
 * @param {条件} query
 * @returns
 */
export function getSpareRequestJoinSpareList(query) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/queryPageList',
        method: 'get',
        params: query
    })
}
/**
 * 从备件导入(物料/工具)
 * @returns
 */
export function importSpares(data) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/importSpares',
        method: 'post',
        data: data
    })
}
/**
 * 从bom导入(物料/工具)
 * @returns
 */
export function importBom(data) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/importBom',
        method: 'post',
        data: data
    })
}
/**
 * 从产品库导入(物料/工具)
 * @returns
 */
export function importProduct(data) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/importProduct',
        method: 'post',
        data: data
    })
}
/**
 * 新建(物料/工具)
 * @returns
 */
export function addSpare(data) {
    return request({
        url: '/system/spareRequestJoinSpareDetail',
        method: 'post',
        data: data
    })
}
/**
 * 校验物料获取工具是否重复(物料/工具)
 * @returns
 */
export function checkSpareOrTool(data) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/check',
        method: 'post',
        data: data
    })
}
/**
 * 编辑(物料/工具)
 * @returns
 */
export function editSpare(data) {
    return request({
        url: '/system/spareRequestJoinSpareDetail',
        method: 'put',
        data: data
    })
}
/**
 * 删除(物料/工具)
 * @returns
 */
export function delSpare(id) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/' + id,
        method: 'delete'
    })
}
/**
 * 详情(物料/工具)
 * @returns
 */
export function getSpareDetail(id) {
    return request({
        url: '/system/spareRequestJoinSpareDetail/' + id,
        method: 'get'
    })
}