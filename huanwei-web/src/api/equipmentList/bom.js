
// bom 管理
import request from "@/utils/request";

/**
 * 表单查询
 * @param 
 * @returns
 */
export function bomManagementGetForm() {
  return request({
    url: '/system/bomManagement/getForm',
    method: 'get',
  })
}
export function bomManagementGetForms() {
  return request({
    url: '/system/bomManagement/bomDetail/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function bomManagementList(query) {
  return request({
    url: '/system/bomManagement/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 不分页查询
 * @param {条件} query
 * @returns
 */
export function bomManagementListAll(query) {
  return request({
    // url: '/system/bomManagement/queryPageList',
    url: '/system/bomManagement/queryList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
export function bomManagementAdd(data) {
  return request({
    url: '/system/bomManagement',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function bomManagementEdit(data) {
  return request({
    url: '/system/bomManagement',
    method: 'put',
    data: data
  })
}
/**
 * 修改层级编码验证
 * @returns
 */
export function bomManagementHierarchicalCodingEdit(query) {
  return request({
    url: `/system/bomManagement/checkHierarchicalCoding`,
    method: 'get',
    params: query
  })
}
/**
 * 新增层级编码验证
 * @returns
 */
export function bomManagementHierarchicalCodingAdd() {
  return request({
    url: `/system/bomManagement/checkHierarchicalCoding`,
    method: 'get',
  })
}
/**
 * 删除
 * @returns
 */
export function bomManagementDel(id) {
  return request({
    url: '/system/bomManagement/' + id,
    method: 'delete'
  })
}
/**
 * 一键转为备件
 * @returns
 */
export function bomManagementChangeToSpare(data) {
  return request({
    url: '/system/bomManagement/changeToSpare',
    method: 'put',
    data: data
  })
}
/**
 * 批量删除
 * @returns
 */
export function bomManagementDelbatch(query) {
  return request({
    // url: '/system/bomManagement/batch/' + id,
    url: '/system/bomManagement/bomDel',
    method: 'delete',
    params: query
  })
}
// 编辑表格使用
export function bomManagementDetail(id) {
  return request({
    url: '/system/bomManagement/' + id,
    method: 'get'
  })
}
// 更新层级
export function bomManagementUpdateLevel(id) {
  return request({
    url: `/system/bomManagement/updateLevel?id=${id}`,
    method: 'put'
  })
}
// 获取Bom层级下拉树列表
export function bomManagementTreeSelect(query) {
  return request({
    url: '/system/bomManagement/treeSelect',
    method: 'get',
    params: query
  })
}
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function queryPageListBomInfo(query) {
  return request({
    // url: '/system/equipmentList/queryPageListBomInfo',
    url: '/system/equipmentList/queryPageListSameBom',//合并同类项
    method: 'get',
    params: query
  })
}

/**
 * 工单下 设备bom
 * @param {条件} query
 * @returns
 */
export function workOrderBomInfo(query) {
  return request({
    url: '/workOrder/taskInfo/workOrderEquipmentBoms',
    method: 'get',
    params: query
  })
}
/**
 * 异常项管理 设备bom
 * @param {条件} query
 * @returns
 */
export function excItemBomInfo(query) {
  return request({
    url: '/system/excItem/equipmentBoms',
    method: 'get',
    params: query
  })
}
/**
 * bom管理下面的 使用记录
更换情况统计
 * @param {条件} query
 * @returns
 */
export function partsUseRecordStatistics(query) {
  return request({
    url: `/system/partsUseRecord/statistics`,
    method: 'get',
    params: query
  })
}
/**
 * bom管理下面的 使用记录
 * @param {条件} query
 * @returns
 */
export function partsUseRecordList(query) {
  return request({
    url: `/system/partsUseRecord/list`,
    method: 'get',
    params: query
  })
}
/**
 * bom管理下面的 在用bom
 * @param {条件} query
 * @returns
 */
export function partsInUseList(query) {
  return request({
    url: `/system/partsInUse/list`,
    method: 'get',
    params: query
  })
}
/**
 * 历史设备保存bom
 * @returns
 */
export function historyBomBo(data) {
  return request({
    url: '/system/bomManagement/historyBomBo',
    method: 'post',
    data: data
  })
}
/**
 * 产品保存bom
 * @returns
 */
export function productBomBo(data) {
  return request({
    url: '/system/bomManagement/productBomBo',
    method: 'post',
    data: data
  })
}
/**
 * 拖拽更新bom树
 * @returns
 */
export function updateDeptTreeOrder(data) {
  return request({
    url: '/system/bomManagement/batch',
    method: 'post',
    data: data
  })
}
/**
 * 设备下异常统计
 * @returns
 */
export function equipStatistics(query) {
  return request({
    url: '/system/bomManagement/equipStatistics',
    method: 'get',
    params: query
  })
}
/**
 * 备件更换次数统计
 * @returns
 */
export function replacementStatistics(query) {
  return request({
    url: '/system/partsUseRecord/replacementStatistics',
    method: 'get',
    params: query
  })
}
/**
 * 手动合并bom
 * @returns
 */
export function combineBom(data) {
  return request({
    url: '/system/bomManagement/updateBomBo',
    method: 'post',
    data: data
  })
}
/**
 * 恢复合并bom
 * @returns
 */
export function recoverBom(data) {
  return request({
    url: '/system/bomManagement/recoverBom',
    method: 'post',
    data: data
  })
}
/**
 * 手动新增bom的换下记录
 * @returns
 */
export function addRecord(data) {
  return request({
    url: '/system/bomManagement/addRecord',
    method: 'post',
    data: data
  })
}
/**
 * 手动新增备件的换下记录
 * @returns
 */
export function addRecordSpare(data) {
  return request({
    url: '/dev/sparePartDetailList/addRecord',
    method: 'post',
    data: data
  })
}

/**
 * bom记录批量删除
 * @returns
 */
export function bomRecordDelbatch(id) {
  return request({
    url: '/system/partsUseRecord/' + id,
    method: 'delete',
  })
}
/**
 * bom详情bom记录数据获取 * 
 * @returns
 */
export function bomRecordGetList(query) {
  return request({
    url: '/system/bomManagement/statisticalInformation',
    method: 'get',
    params: query
  })
}