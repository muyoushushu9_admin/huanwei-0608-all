/** 更换记录 */
import request from "@/utils/request";


/**
 * 表单查询
 * @param 
 * @returns
 */
export function materialUsageRecordGetForm() {
    return request({
        url: '/system/materialUsageRecord/getForm',
        method: 'get',
    })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function materialUsageRecordList(query) {
    return request({
        url: '/system/materialUsageRecord/list',
        method: 'get',
        params: query
    })
}
/**
 * 新增
 * @returns
 */
export function materialUsageRecordAdd(data) {
    return request({
        url: '/system/materialUsageRecord',
        method: 'post',
        data: data
    })
}
/**
 * 修改
 * @returns
 */
export function materialUsageRecordEdit(data) {
    return request({
        url: '/system/materialUsageRecord',
        method: 'put',
        data: data
    })
}
/**
 * 删除
 * @returns
 */
export function materialUsageRecordDel(id) {
    return request({
        url: '/system/materialUsageRecord/batch/' + id,
        method: 'delete'
    })
}
// 编辑表格使用
export function materialUsageRecordDetail(id) {
    return request({
        url: '/system/materialUsageRecord/' + id,
        method: 'get'
    })
}
// 维修记录页面查记录详情（通过编码）
export function materialUsageRecordDetailByCode(query) {
    return request({
        url: '/system/materialUsageRecord/getInfoBycode',
        method: 'get',
        params: query
    })
}


/**使用记录列表部分 */
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function materialUsageRecordPageList(query) {
    return request({
        url: '/system/materialUsageRecord/pageList',
        method: 'get',
        params: query
    })
}
/**
 * 表单查询
 * @param 
 * @returns
 */
export function materialUsageRecordGetFormList() {
    return request({
        url: '/system/materialUsageRecord/getFormList',
        method: 'get',
    })
}
/**
 * 左侧树数据获取
 * @param 
 * @returns
 */
export function materialUsageRecordTreeallList(query) {
    return request({
        url: '/system/materialUsageRecord/allList',
        method: 'get',
        params: query
    })
}
/**
 * 使用记录顶部统计区数据
 * @param 
 * @returns
 */
export function materialUsageRecordStatistics(query) {
    return request({
        url: '/system/materialUsageRecord/statistics',
        method: 'get',
        params: query
    })
}


