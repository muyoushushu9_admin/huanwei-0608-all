// 群聊
import request from "@/utils/request";
/**
 * 一键拉群
 * @returns
 */
export function addGroupChat(data) {
    return request({
        url: '/system/requestMessage/addGroupChat',
        method: 'post',
        data: data
    })
}
// 获取群列表（根据每个模块的id）
export function getGroupChatlistData(query) {
    return request({
        url: '/system/requestMessage/queryGroupList',
        method: 'get',
        params: query
    })
}
// 置顶和取消置顶
export function groupChatSetTop(data) {
    return request({
        url: '/system/groupInfo/setTop',
        method: 'put',
        data: data
    })
}
// 修改群名 群头像
export function groupChatUpdateGroup(data) {
    return request({
        url: '/system/groupInfo/updateGroup',
        method: 'put',
        data: data
    })
}
// 获取群成员列表（根据每个模块的id）
export function getGroupChatUserlistData(query) {
    return request({
        url: '/system/groupInfo/queryUserListByGroupId',
        method: 'get',
        params: query
    })
}
// 邀请加入群聊
export function inviteJoinGroupChat(data) {
    return request({
        url: '/system/groupInfo/addGroupChat',
        method: 'put',
        data: data
    })
}
// 退出群聊
export function exitGroupChat(data) {
    return request({
        url: '/system/groupInfo/exitGroupChat',
        method: 'put',
        data: data
    })
}
// 解散群聊
export function dissolveGroupChat(data) {
    return request({
        url: '/system/groupInfo/dissolveGroupChat',
        method: 'put',
        data: data
    })
}
// 群消息发送
export function sendGroupMessage(data) {
    return request({
        url: '/system/groupInfo/sendGroupMessage',
        method: 'put',
        data: data
    })
}
// 群消息列表
export function queryGroupMessageByGroupId(query) {
    return request({
        url: '/system/groupInfo/queryGroupMessageByGroupId',
        method: 'get',
        params: query
    })
}
// 查看消息未读人列表
export function updateReadMessage(data) {
    return request({
        url: '/system/groupInfo/updateReadMessage',
        method: 'put',
        data: data
    })
}