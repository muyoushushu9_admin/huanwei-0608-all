// 动态表格接口
import request from "@/utils/request";

// 编辑表格使用
export function editInterface(id) {
  return request({
    url: '/system/test1/' + id,
    method: 'get'
  })
}
// 文件回显
export function ossLoad(id) {
  return request({
    url: '/system/oss/load',
    method: 'get',
    params: id
  })
}


