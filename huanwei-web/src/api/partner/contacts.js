import request from "@/utils/request";

// 联系人管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
 export function partnerLinkmanGetForm() {
  return request({
    url: '/system/partnerLinkman/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function partnerLinkmanPageList(query) {
  return request({
    url: '/system/partnerLinkman/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function partnerLinkmanAdd(data) {
  return request({
    url: '/system/partnerLinkman',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function partnerLinkmanEdit(data) {
  return request({
    url: '/system/partnerLinkman',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function partnerLinkmanDel(id) {
  return request({
    url: '/system/partnerLinkman/'+id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function partnerLinkmanDelbatch(id) {
  return request({
    url: '/system/partnerLinkman/batch/'+id,
    method: 'delete'
  })
}

// 编辑表格使用
export function partnerLinkmanDetail(id) {
  return request({
    url: '/system/partnerLinkman/' + id,
    method: 'get'
  })
}

