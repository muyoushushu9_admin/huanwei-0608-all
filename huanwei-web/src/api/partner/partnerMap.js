import request from '@/utils/request'

// 合作伙伴地图 - 客户管理
export function customerPartnerMap(query) {
  return request({
    url: '/system/customer/partnerMap',
    method: 'get',
    params: query
  })
}
// 合作伙伴地图 - 供应商
export function supplierPartnerMap(query) {
  return request({
    url: '/system/supplier/partnerMap',
    method: 'get',
    params: query
  })
}

