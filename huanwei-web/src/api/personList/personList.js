import request from "@/utils/request";

// 协同人负责人

/**
 * 表单查询
 * @param 
 * @returns
 */
export function PersonGetTable(query) {
    return request({
        url: '/workOrder/info/queryPersonList',
        method: 'get',
        params: query
    })
}


