import request from "@/utils/request";
// 顶部统计
export function taskListStatistics(query) {
    return request({
        url: '/workOrder/taskInfo/statistics',
        method: 'get',
        params: query
    })
}