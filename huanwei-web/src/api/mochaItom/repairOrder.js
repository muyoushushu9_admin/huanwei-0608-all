import request from "@/utils/request";

// 运维管理——维修工单

/**
 * 表单查询
 * @param 
 * @returns
 */
export function workOrderGetForm() {
	return request({
		url: '/workOrder/info/getForm',
		method: 'get',
	})
}

// 顶部统计
export function workOrderStatistics() {
	return request({
		url: '/workOrder/info/statistics',
		method: 'get'
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function workOrderPageList(query) {
	return request({
		url: '/workOrder/info/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 任务列表分页查询
 * @param {条件} query
 * @returns
 */
export function workOrderPageList1(query) {
	return request({
		url: '/workOrder/taskInfo/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function workOrderInfoAdd(data) {
	return request({
		// url: '/workOrder/info',//仅保存工单
		url: '/workOrder/info/addNew',//工单+任务
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function workOrderInfoEdit(data) {
	return request({
		url: '/workOrder/info',
		method: 'put',
		data: data
	})
}
/**
 * 批量删除
 * @returns
 */
export function workOrderDelbatch(id, formRequest) {

	return request({
		url: '/workOrder/info/batch/' + id,
		method: 'delete',
		params: { formRequest: formRequest }
	})
}
// 详情
export function workOrderInfoDetail(id) {
	return request({
		url: '/workOrder/info/' + id,
		method: 'get'
	})
}

// 工单详情——解决方案
// 详情
export function attachmentUpdate(data) {
	return request({
		url: '/workOrder/info/attachmentUpdate',
		method: 'put',
		data: data
	})
}

// 工单详情 任务部分
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function workTaskInfoPageList(query) {
	return request({
		url: '/workOrder/taskInfo/queryPageList',
		method: 'get',
		params: query
	})
}
// 新增
export function workOrderTaskInfoAdd(data) {
	return request({
		url: '/workOrder/taskInfo',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function workOrderTaskInfoEdit(data) {
	return request({
		url: '/workOrder/taskInfo',
		method: 'put',
		data: data
	})
}/**
 * 批量删除
 * @returns
 */
export function workOrderTaskDelbatch(id, formWork) {
	return request({
		url: '/workOrder/taskInfo/batch/' + id,
		method: 'delete',
		params: { formWork: formWork }
	})
}
// 详情
export function workOrderTaskInfoDetail(id) {
	return request({
		url: '/workOrder/taskInfo/' + id,
		method: 'get'
	})
}
// 前置任务选择
export function prevNextTaskOptions(data) {
	return request({
		url: '/workOrder/taskInfo/prevNextTaskOptions',
		method: 'get',
		params: data
	})
}
// 前置后置任务列表
export function prevNextTaskList(query) {
	return request({
		url: '/workOrder/taskInfo/prevNextTaskList',
		method: 'get',
		params: query
	})
}
// 任务流转日志
export function taskGetLog(query) {
	return request({
		url: '/workOrder/taskInfo/getLog',
		method: 'get',
		params: query
	})
}
// 工单开始功能
export function workStart(data) {
	return request({
		url: '/workOrder/info/start',
		method: 'put',
		params: data
	})
}
// 工单回退功能
export function workBack(data) {
	return request({
		url: '/workOrder/info/back',
		method: 'put',
		params: data
	})
}
// 工单接受功能
export function workAccept(data) {
	return request({
		url: '/workOrder/info/accept',
		method: 'put',
		params: data
	})
}
// 工单 暂停功能
export function workSuspend(data) {
	return request({
		url: '/workOrder/info/suspend',
		method: 'put',
		params: data
	})
}
// 工单 继续功能
export function workGoon(data) {
	return request({
		url: '/workOrder/info/goon',
		method: 'put',
		params: data
	})
}
// 工单 取消
export function workCancel(data) {
	return request({
		url: '/workOrder/info/cancel',
		method: 'put',
		params: data
	})
}
// 工单完成
export function workFinish(data) {
	return request({
		url: '/workOrder/info/finish',
		method: 'put',
		params: data
	})
}
// 工单 分配
export function workAssign(data) {
	return request({
		url: '/workOrder/info/assign',
		method: 'put',
		params: data
	})
}
// 工单 转派
export function workReassign(data) {
	return request({
		url: '/workOrder/info/reassign',
		method: 'put',
		params: data
	})
}
// 工单验收功能
export function checked(data) {
	return request({
		url: '/workOrder/info/checked',
		method: 'put',
		params: data
	})
}
// 子任务部分
// 任务回退功能
export function workBackSon(data) {
	return request({
		url: '/workOrder/taskInfo/back',
		method: 'put',
		params: data
	})
}
// 任务验收功能
export function checkedSon(data) {
	return request({
		url: '/workOrder/taskInfo/checked',
		method: 'put',
		params: data
	})
}
// 任务指派功能
export function assignSon(data) {
	return request({
		url: '/workOrder/taskInfo/assign',
		method: 'put',
		params: data
	})
}
// 任务转派功能
export function reassignSon(data) {
	return request({
		url: '/workOrder/taskInfo/reassign',
		method: 'put',
		params: data
	})
}
// 任务接受功能
export function workAcceptSon(data) {
	return request({
		url: '/workOrder/taskInfo/accept',
		method: 'put',
		params: data
	})
}
//  获取正在执行的任务数量
export function getInProgressCount(query) {
	return request({
		url: '/workOrder/taskInfo/inProgress',
		method: 'get',
		params: query
	})
}

// 子任务开始
export function workBeginSon(data) {
	return request({
		url: '/workOrder/taskInfo/start',
		method: 'put',
		params: data
	})
}
// 子任务 暂停功能
export function workSuspendSon(data) {
	return request({
		url: '/workOrder/taskInfo/suspend',
		method: 'put',
		params: data
	})
}
// 子任务 继续功能
export function workGoonSon(data) {
	return request({
		url: '/workOrder/taskInfo/goon',
		method: 'put',
		params: data
	})
}
// 子任务 取消
export function workCancelSon(data) {
	return request({
		url: '/workOrder/taskInfo/cancel',
		method: 'put',
		params: data
	})
}
// 子任务完成
export function workFinishSon(data) {
	return request({
		url: '/workOrder/taskInfo/finish',
		method: 'put',
		params: data
	})
}
// 子任务 转派
export function workReassignSon(data) {
	return request({
		url: '/workOrder/taskInfo/reassign',
		method: 'put',
		params: data
	})
}
// 任务窗口关闭
export function updateByChild(data) {
	return request({
		url: '/workOrder/info/updateByChild',
		method: 'put',
		params: data
	})
}
// 工单 流转日志
export function workGetLog(data) {
	return request({
		url: '/workOrder/info/getLog',
		method: 'get',
		params: data
	})
}
// 工单任务 巡，点，保养，维修标准项
// export function getStandard(data) {
// 	return request({
// 		url: '/workOrder/taskInfo/getStandard',
// 		method: 'get',
// 		params: data
// 	})
// }

// 工单任务 巡，点，保养，维修标准项
export function getStandard(data) {
	return request({
		url: '/workOrder/taskInfo/getStandard',
		method: 'get',
		params: data
	})
}


// 解决方案列表
export function orderSolutionList(data) {
	return request({
		url: '/system/orderSolution/queryPageList',
		method: 'get',
		params: data
	})
}

// 解决方案新增
export function orderSolutionAdd(data) {
	return request({
		url: '/system/orderSolution',
		method: 'post',
		data: data
	})
}

// 详情
export function orderSolutionDetail(id) {
	return request({
		url: '/system/orderSolution/' + id,
		method: 'get'
	})
}



export function orderSolutionPut(data) {
	return request({
		url: '/system/orderSolution',
		method: 'put',
		data: data
	})
}

/**
 * 删除
 * @returns
 */
export function orderSolutionDel(id) {
	return request({
		url: '/system/orderSolution/batch/' + id,
		method: 'delete'
	})
}
//  工单回执 新增
export function receiptAdd(data) {
	return request({
		url: '/workOrder/receipt',
		method: 'post',
		data: data
	})
}

// 工单回执查询
export function receiptDetail(id) {
	return request({
		url: '/workOrder/receipt/' + id,
		method: 'get',
	})
}

// 工单回执  获取故障处理人接口
export function faultHandler(data) {
	return request({
		url: '/workOrder/receipt/faultHandler',
		method: 'get',
		params: data
	})
}

// 任务计划-列表接口
export function taskPlanList(data) {
	return request({
		url: '/system/taskInfoPlan/queryPageList',
		method: 'get',
		params: data
	})
}

// 任务计划-保存接口-新建
export function taskPlanAdd(data) {
	return request({
		url: '/system/taskInfoPlan',
		method: 'post',
		data: data
	})
}

// 任务计划-保存接口-编辑
export function taskPlanEdit(data) {
	return request({
		url: '/system/taskInfoPlan',
		method: 'put',
		data: data
	})
}

// 任务计划-获取详情
export function taskPlanDetail(data) {
	return request({
		url: `/system/taskInfoPlan/${data.id}`,
		method: 'get',
		data: data
	})
}

// 任务计划-获取详情
export function taskPlanDelete(id) {
	return request({
		url: `/system/taskInfoPlan/${id}`,
		method: 'delete'
	})
}

// 任务计划-修改状态
export function swtichStatus(data) {
	return request({
		url: `/system/taskInfoPlan/changeStatus`,
		method: 'put',
		data
	})
}
// 负责人接受任务计划信息
export function acceptStatus(data) {
	return request({
		url: `/system/taskInfoPlan/accept?id=${data.id}&status=${data.status}`,
		method: 'put',
	})
}
// 工单管理-统计数据（全部，我创建的，我审批的，我负责的，我协同的）
export function repairOrderStatistics() {
	return request({
		url: `/workOrder/info/myStatistics`,
		method: 'get'
	})
}
// cron表达式翻译成文字
export function translateCron(query) {
	return request({
		url: `/system/taskInfoPlan/cron`,
		method: 'get',
		params: query
	})
}

// 计划的顶部统计数据
export function getTaskInfoPlaStatistics() {
	return request({
		url: `/system/taskInfoPlan/statistics`,
		method: 'get'
	})
}
// 用工信息数据获取
export function taskUserInfo(query) {
	return request({
		url: `/workOrder/taskUserInfo/queryPageList`,
		method: 'get',
		params: query
	})
}
// 打印模板
export function generatePointReport(equipmentId, taskId) {
	return request({
		url: `/system/orderTaskItemInfo/generatePointReport?equipmentId=${equipmentId}&taskId=${taskId}`,
		method: 'post',

	})
}
// 日报信息数据获取
export function getServiceReportList(query) {
	return request({
		url: `/workOrder/serviceReport/queryPageList`,
		method: 'get',
		params: query
	})
}
// 日报详情获取
export function getServiceReportDetail(data) {
	return request({
		url: `/workOrder/serviceReport/${data.id}?reportDate=${data.reportDate}`,
		method: 'get',
		data: data
	})
}