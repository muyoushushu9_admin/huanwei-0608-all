import request from "@/utils/request";

// 客户管理api

/**
 * 表单查询
 * @param 
 * @returns
 */
export function requestInfoGetForm() {
	return request({
		url: '/system/requestInfo/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function requestInfoPageList(query) {
	return request({
		url: '/system/requestInfo/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function requestInfoAdd(data) {
	return request({
		url: '/system/requestInfo',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function requestInfoEdit(data) {
	return request({
		url: '/system/requestInfo',
		method: 'put',
		data: data
	})
}
/**
 * 删除
 * @returns
 */
export function requestInfoDel(id) {
	return request({
		url: '/system/requestInfo/' + id,
		method: 'delete'
	})
}
/**
 * 批量删除
 * @returns
 */
export function requestInfoDelbatch(id) {
	return request({
		url: '/system/requestInfo/batch/' + id,
		method: 'delete'
	})
}
// 详情
export function requestInfoDetail(id) {
	return request({
		url: '/system/requestInfo/' + id,
		method: 'get'
	})
}

// 顶部统计
export function requestInfoStatistics() {
	return request({
		url: '/system/requestInfo/statistics',
		method: 'get'
	})
}

/**
 * 查询服务请求信息进度列表
 * @param 
 * @returns
 */
export function requestInfoScheduleList(data) {
	return request({
		url: '/system/requestInfoSchedule/list',
		method: 'get',
		params: data
	})
}

/**
 * 服务请求流程流转
 * @returns
 */
export function requestInfoProcessFlow(data) {
	return request({
		url: '/system/requestInfo/processFlow',
		method: 'put',
		params: data
	})
}


// // 复制项目
// export function copySysProjectList(data) {
// 	return request({
// 		url: '/system/requestInfo/copySysProjectList',
// 		method: 'post',
// 		params: data
// 	})
// }

// // 归档项目
// export function archiveProjectList(data) {
// 	return request({
// 		url: '/system/requestInfo/archiveProjectList',
// 		method: 'put',
// 		params: data
// 	})
// }
// // 我方项目成员查询
// export function requestInfoOur(id) {
// 	return request({
// 		url: '/system/requestInfo/our/' + id,
// 		method: 'get'
// 	})
// }
// // 合作方项目成员查询
// export function requestInfoPartner(id) {
// 	return request({
// 		url: '/system/requestInfo/partner/' + id,
// 		method: 'get'
// 	})
// }
// // 修改我方项目成员
// export function editOurProjectMember(data) {
// 	return request({
// 		url: '/system/requestInfo/editOurProjectMember',
// 		method: 'put',
// 		params: data
// 	})
// }
// // 项目历程
// export function queryListByMonth(data) {
// 	return request({
// 		url: '/system/serviceHistory/queryListByMonth/?' + data,
// 		method: 'get',
// 	})
// }
