import request from "@/utils/request";

// 运维管理——异常项管理

/**
 * 表单查询
 * @param 
 * @returns
 */
export function excItemGetForm() {
	return request({
		url: `/system/excItem/getForm`,
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function excItemPageList(query) {
	return request({
		url: '/system/excItem/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function excItemAdd(data) {
	return request({
		url: '/system/excItem',
		method: 'post',
		data: data
	})
}

// 顶部统计
export function excItemStatistics() {
	return request({
		url: '/system/excItem/statistics',
		method: 'get'
	})
}
/**
 * 修改
 * @returns
 */
export function excItemEdit(data) {
	return request({
		url: '/system/excItem',
		method: 'put',
		data: data
	})
}
/**
 * 批量删除
 * @returns
 */
export function excItemDelbatch(id) {
	return request({
		url: '/system/excItem/' + id,
		method: 'delete'
	})
}
// 详情
export function excItemDetail(id) {
	return request({
		url: '/system/excItem/' + id,
		method: 'get'
	})
}

// 关联异常项(根据设备编码)
export function abnormals(query) {
	return request({
		url: '/system/excItem/getEquipBom',
		method: 'get',
		params: query
	})
}

/**
 * 批量报修
 * @returns
 */
export function batchRepairs(id) {
	return request({
		url: '/system/excItem/repairs/' + id,
		method: 'put'
	})
}



/**
 * 查询设备的故障个数
 * @returns
 */
export function getEquipBom(query) {
	return request({
		url: '/system/excItem/getEquipBom',
		method: 'get',
		params: query
	})
}



/**
 * 异常项页面添加工单
 * @returns
 */
export function excItemRepairs(id, data) {
	return request({
		url: '/system/excItem/repairs?ids=' + id,
		method: 'put',
		data: data
	})
}
/**
 * 异常项表格分页查询
 * @param {条件} query
 * @returns
 */
export function excItemPageListTable(query) {
	return request({
		url: '/system/excItem/unRepairingList',
		method: 'get',
		params: query
	})
}

/**
 * 手动消除
 * @param {条件} query
 * @returns
 */
export function clear(id,resean) {
	return request({
		url: `/system/excItem/clears/${id}?eliminateRemarks=${resean}`,
		method: 'put'
	})
}