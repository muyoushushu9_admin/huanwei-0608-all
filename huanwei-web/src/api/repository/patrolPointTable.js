import request from "@/utils/request";

// 巡点检
// 巡点检 列表查询
export function patrolInspectionTaskItemInfo(query) {
  return request({
    url: '/system/patrolInspectionTaskItemInfo/list',
    method: 'get',
    params: query
  })
}
// 点检 列表查询
export function patrolPointTaskItemInfo(query) {
  return request({
    url: '/system/pointInspectionTaskItemInfo/list',
    method: 'get',
    params: query
  })
}

/**
 * 新增
 * @returns
 */
export function patrolInspectionTaskItemInfoAdd(data) {
  return request({
    url: '/system/patrolInspectionTaskItemInfo',
    method: 'post',
    data: data
  })
}
/**
 * 点检标准新增
 * @returns
 */
export function pointInspectionTaskItemInfoAdd(data) {
  return request({
    url: '/system/pointInspectionTaskItemInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function patrolInspectionTaskItemInfoEdit(data) {
  return request({
    url: '/system/patrolInspectionTaskItemInfo',
    method: 'put',
    data: data
  })
}
/**
 * 点检修改
 * @returns
 */
export function pointInspectionTaskItemInfoEdit(data) {
  return request({
    url: '/system/pointInspectionTaskItemInfo',
    method: 'put',
    data: data
  })
}
// 编辑表格使用
export function patrolInspectionTaskItemInfoDetail(id) {
  return request({
    url: '/system/patrolInspectionTaskItemInfo/' + id,
    method: 'get'
  })
}
// 点检编辑表格使用
export function pointInspectionTaskItemInfoDetail(id) {
  return request({
    url: '/system/pointInspectionTaskItemInfo/' + id,
    method: 'get'
  })
}
/**
 * 删除
 * @returns
 */
export function patrolInspectionTaskItemInfoDel(id) {
  return request({
    url: '/system/patrolInspectionTaskItemInfo/' + id,
    method: 'delete'
  })
}
/**
 * 点检删除
 * @returns
 */
export function pointInspectionTaskItemInfoDel(id) {
  return request({
    url: '/system/pointInspectionTaskItemInfo/' + id,
    method: 'delete'
  })
}