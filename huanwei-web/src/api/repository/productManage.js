import request from "@/utils/request";

// 产品库

/**
 * 表单查询
 * @param 
 * @returns
 */
export function workOrderGetForm() {   //查询列表数据
	return request({
		url: '/system/product/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function workOrderPageList(query) {   //表格数据  
	return request({
		url: '/system/product/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function workOrderInfoAdd(data) {    //添加
	return request({
		url: '/system/product',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function workOrderInfoEdit(data) {  //修改
	return request({
		url: '/system/product',
		method: 'put',
		data: data
	})
}

export function workOrderIndel(id) {     //单个删除    
	return request({
		url: '/system/product' + id,
		method: 'delete'
	})
}
/**
 * 批量删除
 * @returns
 */
export function workOrderDelbatch(ids) {    //批量删除
	return request({
		url: '/system/product/batch/' + ids,
		method: 'delete'
	})
}
// 详情
export function workOrderInfoDetail(id) {    //详情
	return request({
		url: '/system/product/' + id,
		method: 'get'
	})
}

/**
 * 动态表单品牌组件使用的品牌列表
 * @param {条件} query
 * @returns
 */
export function workOrderpage(query) {
	return request({
		url: '/system/product/ProductPage',
		method: 'get',
		params: query
	})
}


// 改动后接口
/**
 * 表单查询
 * @param 
 * @returns
 */
export function fixProductGetForm() {   //查询列表数据
	return request({
		url: '/system/fixProduct/getForm',
		method: 'get',
	})
}
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function fixProductPageList(query) {   //表格数据  
	return request({
		url: '/system/fixProduct/queryPageList',
		method: 'get',
		params: query
	})
}
// 详情
export function fixProductInfoDetail(id) {    //详情
	return request({
		url: '/system/fixProduct/' + id,
		method: 'get'
	})
}
// 详情
export function fixProductInfoDetailModel(model) {    //详情
	return request({
		url: '/system/fixProduct/' + model,
		method: 'get'
	})
}


// 品类下拉树
export function categoryTreeSelect() {   //查询列表数据
	return request({
		url: '/system/formTreeSelectComponentData/treeSelect',
		method: 'get',
	})
}
// 品牌选择
export function brandList() {   //查询列表数据
	return request({
		url: '/system/brand/queryList',
		method: 'get',
	})
}

// 编辑产品
export function fixProductDetail(data) {
	return request({
		url: '/system/fixProduct',
		method: 'put',
		data: data
	})
}
// 新增产品
export function fixProductDetailAdd(data) {
	return request({
		url: '/system/fixProduct',
		method: 'post',
		data: data
	})
}



// 编辑表格使用
export function inspectDetail(id) {
	return request({
		url: '/system/patrolInspectionTaskInfo/' + id,
		method: 'get'
	})
}
// 获取点击或搜索次数较多的品类品牌在筛选区显示
export function getCntList(query) {
	return request({
		url: '/system/fixProduct/getBrandAndCategory',
		method: 'get',
		params: query
	})
}
// 获取点击搜索后拿到的品类品牌在筛选区显示
export function getBrandAndCategoryForProduct(query) {
	return request({
		url: '/system/fixProduct/getBrandAndCategoryForProduct',
		method: 'get',
		params: query
	})
}

// 新增产品公共图片视频
export function addProductCommonFile(data) {
	return request({
		url: '/system/productCommonFile',
		method: 'post',
		data: data
	})
}
// 查询产品公共图片视频
export function getProductCommonFileQueryList(query) {
	return request({
		url: '/system/productCommonFile/queryList',
		method: 'get',
		params: query
	})
}
/**
 * 批量删除产品公共图片视频
 * @returns
 */
export function delProductCommonFile(query) {
	return request({
		url: '/system/productCommonFile/batch/del',
		method: 'delete',
		params: query
	})
}
/**
 * 新增或者编辑产品时打开页面就获取类型，计量单位，产品状态 的值
 * @returns
 */
export function getConfigureProductCommonAttributes() {
	return request({
		url: '/lib/attributeDict/getConfigureProductCommonAttributes',
		method: 'get',
	})
}