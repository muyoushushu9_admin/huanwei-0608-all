import request from "@/utils/request";

// 品类库api

/**
 * 左侧的类型查询
 * @param 
 * @returns
 */
export function queryCategoryTree() {
	return request({
		url: '/lib/category/queryCategoryTree',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function categoryPage(query) {
	return request({
		url: '/lib/category/page',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function partnerLinkmanAdd(data) {
	return request({
		url: '/system/partnerLinkman',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function partnerLinkmanEdit(data) {
	return request({
		url: '/system/partnerLinkman',
		method: 'put',
		data: data
	})
}
/**
 * 删除
 * @returns
 */
export function partnerLinkmanDel(id) {
	return request({
		url: '/system/partnerLinkman/' + id,
		method: 'delete'
	})
}

// 品牌 批量删除
export function docDelbatch(id, templateId) {
	return request({
		url: '/lib/template/batch/' + id + '?templateId=' + templateId,
		method: 'delete'
	})
}

// 品牌编辑 
export function templatedEdit(data) {
	return request({
		url: '/lib/template/update',
		method: 'put',
		data: data
	})
}



// 编辑表格使用
export function partnerLinkmanDetail(id) {
	return request({
		url: '/system/partnerLinkman/' + id,
		method: 'get'
	})
}


/**
 * 查询模板库企业字段列表
 * @param {条件} query
 * @returns
 */
export function templateCompanyFieldQueryList(query) {
	return request({
		url: '/system/templateCompanyField/queryList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function templateCompanyFieldAdd(data) {
	return request({
		url: '/system/templateCompanyField',
		method: 'post',
		data: data
	})
}

/**
 * 修改
 * @returns
 */
export function templateCompanyFieldEdit(data) {
	return request({
		url: '/system/templateCompanyField',
		method: 'put',
		data: data
	})
}
/**
 * 模板库筛选字段修改最新
 * @returns
 */
export function configureProductAttributes(data) {
	return request({
		url: '/lib/attributeDict/configureProductAttributes',
		method: 'post',
		data: data
	})
}
/**
 * 品类数据数据 查询
 * @param {条件} query
 * @returns
 */
export function templateCategoryPage(query) {
	return request({
		url: '/lib/template/queryPageList',
		method: 'get',
		params: query
	})
}

/**
 * 新增
 * @returns
 */
export function templateCategoryAdd(data) {
	return request({
		url: '/lib/template/add',
		method: 'post',
		data: data
	})
}


/**
 * 表单详情查询
 * @returns
 */
export function templateDetail(query) {
	return request({
		url: '/lib/template/queryById',
		method: 'get',
		params: query
	})
}

//品类管理

/**
 * 列表
 * @returns
 */
export function categoryList(query) {
	return request({
		url: '/lib/category/list',
		method: 'get',
		params: query
	})
}

/**
 * 新增
 * @returns
 */
export function categoryAdd(data) {
	return request({
		url: '/lib/category/addSysCategory',
		method: 'post',
		data: data
	})
}

/**
 * 删除
 * @returns
 */
export function categoryDel(id) {
	return request({
		url: `/lib/category/delSysCategory/${id}`,
		method: 'delete',
	})
}


/**
 * 修改
 * @returns
 */
export function categoryUp(data) {
	return request({
		url: '/lib/category/editSysCategory',
		method: 'put',
		data: data
	})
}


// 查询品类管理列表（排除节点）
export function listCategoryExcludeChild(id) {
	return request({
		url: '/lib/category/list/exclude/' + id,
		method: 'get'
	})
}


/**
 * 动态表单品类组件使用的品类列表
 * @returns
 */
export function categoryPageTable(query) {
	return request({
		url: '/system/formTreeSelectComponentData/categoryPage',
		method: 'get',
		params: query
	})
}

/**
 * 同步
 * @returns
 */
export function synchrodata(data) {
	return request({
		url: '/system/formTreeSelectComponentData/synchrodata',
		method: 'put',
		params: data
	})
}

/**
 * 品类品牌 产品列表 分页查询
 * @param {条件} query
 * @returns
 */
export function fixProductPageList(query) {
	return request({
		url: '/system/fixProduct/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 删除
 * @returns
 */
export function fixProductDel(ids) {
	return request({
		url: `/system/fixProduct/batch/` + ids,
		method: 'delete',
	})
}

// 详情
export function fixProductDetail(id) {
	return request({
		url: '/system/fixProduct/' + id,
		method: 'get'
	})
}
//设备管理 -》详情-》配置专有属性保存，
export function exclusiveAttributes(data, id, type) {
	return request({
		url: `/system/templateCompanyField/exclusiveAttributes?id=${id}&type=${type}`,
		method: 'post',
		data: data
	})
}
//设备管理 -》详情-》配置专有属性保存，
export function exclusiveAttributesNew(data, id, type) {
	return request({
		url: `/system/projectAttribute`,
		method: 'post',
		data: data
	})
}

/**
 * 左侧的类型查询
 * @param 
 * @returns
 */
export function selectCategoryTree(data) {
	return request({
		url: '/lib/category/selectCategoryTree',
		method: 'get',
		params: data
	})
}

/**
 * 左侧的类型查询 子集查询
 * @param 
 * @returns
 */
export function selectCategoryTreeLeaf(data) {
	return request({
		url: '/lib/category/selectCategoryTree',
		method: 'get',
		params: data
	})
}
