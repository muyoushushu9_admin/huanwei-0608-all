import request from "@/utils/request";

// 保养标准

/**
 * 表单查询
 * @param 
 * @returns
 */
 export function inspectGetForm() {
  return request({
    url: '/system/patrolInspectionTaskInfo/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function inspectPageList(query) {
  return request({
    url: '/system/patrolInspectionTaskInfo/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function inspectAdd(data) {
  return request({
    url: '/system/patrolInspectionTaskInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function inspectEdit(data) {
  return request({
    url: '/system/patrolInspectionTaskInfo',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function inspectDel(id) {
  return request({
    url: '/system/patrolInspectionTaskInfo/'+id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function inspectDelbatch(id) {
  return request({
    url: '/system/patrolInspectionTaskInfo/batch/'+id,
    method: 'delete'
  })
}
// 编辑表格使用
export function inspectDetail(id) {
  return request({
    url: '/system/patrolInspectionTaskInfo/' + id,
    method: 'get'
  })
}




