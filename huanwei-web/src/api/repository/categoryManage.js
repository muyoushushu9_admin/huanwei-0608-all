import request from "@/utils/request";
/**
 * 表单查询
 * @param 
 * @returns
 */
export function libCategoryGetForm() {
	return request({
		url: '/system/formTreeSelectComponentData/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function queryPageList(query) {
	return request({
		url: '/system/formTreeSelectComponentData/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function addList(data) {
	return request({
		url: '/system/formTreeSelectComponentData/addList',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function categoryEdit(data) {
	return request({
		url: '/system/formTreeSelectComponentData/editList',
		method: 'put',
		data: data
	})
}
/**
 * 删除
 * @returns
 */
export function categoryDel(id) {
	return request({
		url: '/system/formTreeSelectComponentData/deleteList/' + id,
		method: 'delete'
	})
}
/**
 * 批量删除
 * @returns
 */
export function categoryDelbatch(id) {
	return request({
		url: '/system/formTreeSelectComponentData/batch/deleteList/' + id,
		method: 'delete'
	})
}
// 详情
export function categoryDetail(id) {
	return request({
		url: '/system/formTreeSelectComponentData/' + id,
		method: 'get'
	})
}

/**
 * 动态表单品牌组件使用的品牌列表
 * @param {条件} query
 * @returns
 */
export function brandPage(query) {
	return request({
		url: '/system/brand/brandPage',
		method: 'get',
		params: query
	})
}
/**
 * 品类编码关联产品就不能修改
 * @param {条件} query
 * @returns
 */
export function noEdit(query) {
	return request({
		url: '/system/formTreeSelectComponentData/notUpdate',
		method: 'get',
		params: query
	})
}
