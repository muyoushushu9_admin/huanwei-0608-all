import request from "@/utils/request";

// 保养标准

/**
 * 表单查询
 * @param 
 * @returns
 */
 export function pointGetForm() {
  return request({
    url: '/system/pointInspectionTaskInfo/getForm',
    method: 'get',
  })
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
 export function pointPageList(query) {
  return request({
    url: '/system/pointInspectionTaskInfo/queryPageList',
    method: 'get',
    params: query
  })
}
/**
 * 新增
 * @returns
 */
 export function pointAdd(data) {
  return request({
    url: '/system/pointInspectionTaskInfo',
    method: 'post',
    data: data
  })
}
/**
 * 修改
 * @returns
 */
export function pointEdit(data) {
  return request({
    url: '/system/pointInspectionTaskInfo',
    method: 'put',
    data: data
  })
}
/**
 * 删除
 * @returns
 */
export function pointDel(id) {
  return request({
    url: '/system/pointInspectionTaskInfo/'+id,
    method: 'delete'
  })
}
/**
 * 批量删除
 * @returns
 */
export function pointDelbatch(id) {
  return request({
    url: '/system/pointInspectionTaskInfo/batch/'+id,
    method: 'delete'
  })
}
// 编辑表格使用
export function pointDetail(id) {
  return request({
    url: '/system/pointInspectionTaskInfo/' + id,
    method: 'get'
  })
}




