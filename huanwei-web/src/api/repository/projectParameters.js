import request from "@/utils/request";

// 联系人管理api

/**
 * 左侧的类型查询
 * @param 
 * @returns
 */
export function queryProjectParameterTree() {
	return request({
		url: '/system/projectArchives/queryProjectParameterTree',
		method: 'get',
	})
}

/**
 * 查询模板库企业配置
 * @param {条件} query
 * @returns
 */
export function templateEnterpriseConfig(query) {
	return request({
		url: '/system/templateEnterpriseConfig/queryList',
		method: 'get',
		params: query
	})
}

export function templateEnterpriseConfigProj(id) {
	return request({
		url: '/system/projectList/queryProjectTemplateConfig?projectId=' + id,
		method: 'post',
	})
}
// 保存项目下的专有属性
export function templateEnterpriseConfigProjAdd(query) {
	return request({
		url: '/system/projectList/queryProjectTemplateConfig',
		method: 'put',
		data: query
	})
}
// 更新项目下的专有属性
export function templateEnterpriseConfigProjUpdate(query) {
	return request({
		url: '/system/projectList/queryProjectTemplateConfig',
		method: 'post',
		data: query
	})
}
/**
 * 新增模板库企业配置
 * @returns 
 */
export function templateEnterpriseConfigAdd(data) {
	return request({
		url: '/system/templateEnterpriseConfig',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function templateEnterpriseConfigUpdata(data) {
	return request({
		url: '/system/templateEnterpriseConfig',
		method: 'put',
		data: data
	})
}
/**
 * 删除模板库企业配置
 * @returns
 */
export function templateEnterpriseConfigDel(id) {
	return request({
		url: '/system/templateEnterpriseConfig/' + id,
		method: 'delete'
	})
}

/**
 * 模板配置的项目参数的表单或者列表数据的新增
 * @returns
 */
export function templateTypeAdd(data) {
	return request({
		url: '/lib/template/add',
		method: 'post',
		data: data
	})
}


/**
 * 模板配置的项目参数的表单或者列表数据的查询
 * @returns
 */
export function templateSelect(query) {
	return request({
		url: '/lib/template/queryById',
		method: 'get',
		params: query
	})
}
// 保存字段
export function templateSelectNew(query) {
	return request({
		url: '/system/projectAttribute/query',
		method: 'get',
		params: query
	})
}
// 删除模板
export function templateDelete(id) {
	return request({
		url: `/system/projectList/config/batch/${id}`,
		method: 'delete',
	})
}
/**
 * 模板配置的项目参数的表单或者列表数据的修改
 * @returns
 */
export function templateUpdata(data) {
	return request({
		url: '/lib/template/update',
		method: 'put',
		data: data
	})
}


/**
 * 模板配置的项目参数的列表数据的查询
 * @returns
 */
export function templateTableSelect(data) {
	return request({
		url: '/lib/template/queryPageList',
		method: 'get',
		params: data
	})
}

/**
 * 删除模板库企业配置
 * @returns
 */
export function templateTableDel(data) {
	return request({
		url: '/lib/template/delete',
		method: 'delete',
		params: data
	})
}

/**
 * 模板库筛选字段修改
 * @returns
 */
export function userFilterFieldEdit(data) {
	return request({
		url: '/lib/userFilterField/edit',
		method: 'post',
		params: data
	})
}



/**
 * 模板库筛选字段查询
 * @returns
 */
export function userFilterFieldList(data) {
	return request({
		url: '/lib/userFilterField/list',
		method: 'get',
		params: data
	})
}





/**
 * 动态表单下拉 查询
 * @returns
 */
export function formTreeSelectComponentDataPage(data) {
	return request({
		url: '/system/formTreeSelectComponentData/page',
		method: 'get',
		params: data
	})
}



/**
 * 动态表单下拉 添加
 * @returns
 */
export function formTreeSelectComponentDataAdd(data) {
	return request({
		url: '/system/formTreeSelectComponentData/add',
		method: 'post',
		data: data
	})
}


/**
 * 传数组查配置集合
 * @returns
 */
export function sysTemplateByProjectParameter(data) {
	return request({
		url: '/system/projectList/sysTemplateByProjectParameter',
		method: 'get',
		params: data
	})
}

/**
 * 模板配置的项目参数的表单或者列表数据的新增
 * @returns
 */
export function templateTypeAddS(data) {
	return request({
		url: '/system/fixProduct',
		method: 'post',
		data: data
	})
}
export function templateTypeAddSNew(data) {
	return request({
		url: '/system/projectAttribute ',
		method: 'put',
		data: data
	})
}

// 批量保存數據
export function templateTypeAddBatch(data) {
	return request({
		url: '/system/projectAttribute/batch',
		method: 'put',
		data: data
	})
}
// 批量保存數據-
export function templateTypeUpdateBatch(data) {
	return request({
		url: '/system/projectAttribute/batch',
		method: 'post',
		data: data
	})
}
// 批量保存字段
export function templateTypeSave(data) {
	return request({
		url: '/system/projectList/updateProjectTemplateConfig',
		method: 'post',
		data: data
	})
}
// 批量保存字段
export function templateTypeUpfate(data) {
	return request({
		url: '/system/projectList/updateProjectTemplateConfig',
		method: 'post',
		data: data
	})
}
export function templateUpdataS(data) {
	return request({
		url: '/system/fixProduct',
		method: 'put',
		data: data
	})
}
/**
 * 设备管理 -》详情-》从模板获取品类表单  
 */
export function queryListByEquipment(query) {
	return request({
		url: '/system/templateEnterpriseConfig/queryListByEquipmentId',
		method: 'get',
		params: query
	})
}
/**
 * 设备管理 -》详情-》从模板获取品类表单  
 */
export function queryListByProject(query) {
	return request({
		url: '/system/templateEnterpriseConfig/queryList',
		method: 'get',
		params: query
	})
}
/**
 * 设备管理 -》详情-》从模板获取品类表单  新接口
 */
export function queryListByEquipmentNew(query) {
	return request({
		url: '/system/projectAttribute/query',
		method: 'get',
		params: query
	})
}
/**
 * 设备管理 -》详情-》保存设备专有属性
 */
export function updateExclusiveAttributesDynamicFields(data) {
	return request({
		url: '/system/equipmentList/updateExclusiveAttributesDynamicFields',
		method: 'put',
		data: data
	})
}
/**
 * 设备管理 -》详情-》保存设备专有属性 新接口
 */
export function updateExclusiveAttributesDynamicFieldsNew(data) {
	return request({
		url: '/system/projectAttribute',
		method: 'put',
		data: data
	})
}
// //bom管理 -》详情-》编辑专有属性保存，
export function updateExclusiveAttributesDynamicFieldsbom(data) {
	return request({
		url: `/system/bomManagement/updateExclusiveAttributesDynamicFields`,
		method: 'put',
		data: data
	})
}
// //备件管理 -》详情-》编辑专有属性保存，
export function updateExclusiveAttributesDynamicFieldsspare(data) {
	return request({
		url: `/dev/sparePartDetailList/updateExclusiveAttributesDynamicFields`,
		method: 'put',
		data: data
	})
}

// 项目列表  引用模板
export function referenceTemplate(query) {
	return request({
		url: '/system/projectList/referenceTemplate',
		method: 'post',
		params: query
	})
}
// 项目列表  删除
export function projectAttributeDelete(id) {
	return request({
		url: '/system/projectAttribute/' + id,
		method: 'delete'
	})
}