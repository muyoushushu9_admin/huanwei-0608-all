import request from "@/utils/request";
/**
 * 表单查询
 * @param 
 * @returns
 */
export function brandGetForm() {
	return request({
		url: '/system/brand/getForm',
		method: 'get',
	})
}

/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function queryPageList(query) {
	return request({
		url: '/system/brand/queryPageList',
		method: 'get',
		params: query
	})
}
/**
 * 新增
 * @returns
 */
export function brandAdd(data) {
	return request({
		url: '/system/brand',
		method: 'post',
		data: data
	})
}
/**
 * 修改
 * @returns
 */
export function brandEdit(data) {
	return request({
		url: '/system/brand',
		method: 'put',
		data: data
	})
}
/**
 * 品牌删除
 * @returns
 */
export function brandDel(id) {
	return request({
		url: '/system/brand/' + id,
		method: 'delete'
	})
}

// 品类删除
export function brandDelte(id) {
	return request({
		url: '/system/categoryBrand/' + id,
		method: 'delete'
	})
}
/**
 * 批量删除
 * @returns
 */
export function brandDelbatch(id) {
	return request({
		url: '/system/brand/batch/' + id,
		method: 'delete'
	})
}
// 详情
export function brandDetail(id) {
	return request({
		url: '/system/brand/' + id,
		method: 'get'
	})
}
/**
 * 分页查询
 * @param {条件} query
 * @returns
 */
export function bRqueryList(data) {
	return request({
		url: '/system/categoryBrand/queryList/' + data,
		method: 'get',
	})
}
/**
 * 品牌选中
 * @returns
 */
export function categoryBrandEdit(data) {
	return request({
		url: '/system/categoryBrand',
		method: 'put',
		data: data
	})
}
/**
 * 新增
 * @returns
 */
export function categoryBrandAdd(data) {
	return request({
		url: '/system/categoryBrand/addProduct',
		method: 'post',
		data: data
	})
}
