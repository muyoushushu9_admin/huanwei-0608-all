import request from "@/utils/request";

//模板库 品牌商管理

/**
 * 表单查询
 * @param 
 * @returns
 */
export function brandOwnerManagementGetForm() {
    return request({
        url: '/system/partner/getForm',
        method: 'get',
    })
}

/**
 * 查询公司列表 分页查询
 * @param 
 * @returns
 */
export function queryBrandOwnerManagementList(query) {
    return request({
        url: '/system/partner/queryPageList',
        method: 'get',
        params: query
    })
}
/**
 * 公司列表 详情
 * @param 
 * @returns
 */
export function detailBrandOwnerManagementList(id) {
    return request({
        url: '/system/partner/' + id,
        method: 'get'
    })
}

/**
 * 新增
 * @returns
 */
export function brandOwnerManagementAdd(data) {
    return request({
        url: '/system/partner',
        method: 'post',
        data: data
    })
}
/**
 * 修改
 * @returns
 */
export function brandOwnerManagementEdit(data) {
    return request({
        url: '/system/partner',
        method: 'put',
        data: data
    })
}
/**
 * 删除
 * @returns
 */
export function brandOwnerManagementDel(id) {
    return request({
        url: '/system/partner/' + id,
        method: 'delete'
    })
}
/**
 * 批量删除
 * @returns
 */
export function brandOwnerManagementbatch(id) {
    return request({
        url: '/system/partner/batch/' + id,
        method: 'delete'
    })
}

/**
 * 查询供应商列表 只查供应商
 * @param 
 * @returns
 */
export function getComponryList(query) {
    return request({
        url: '/system/partner/supplierList',
        method: 'get',
        params: query
    })
}
