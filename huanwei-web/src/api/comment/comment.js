// 评论信息
import request from "@/utils/request";

/**
 * 新增评论
 * @returns
 */
export function requestMessageAdd(data) {
    return request({
        url: '/system/requestMessage',
        method: 'post',
        data: data
    })
}

/**
* 查询评论信息
* @param {条件} query
* @returns
*/
export function getRequestMessageList(query) {
    return request({
        // url: '/system/requestMessage/queryPageList',
        url: '/system/requestMessage/queryListTree',
        method: 'get',
        params: query
    })
}
/**
 * 删除评论
 * @returns
 */
export function requestMessageDel(id) {
    return request({
        url: '/system/requestMessage/' + id,
        method: 'delete'
    })
}
/**
 * 评论已读情况
 * @returns
 */
export function requestMessageRead(data) {
    return request({
        url: '/system/requestMessage/updateReadMessage',
        method: 'post',
        data: data
    })
}