import request from '@/utils/request'
import { parseStrEmpty } from "@/utils/ruoyi";

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: query
  })
}
// 顶部统计
export function userStatistics() {
  return request({
    url: '/system/user/list?pageNum=1&pageSize=10  ',
    method: 'get'
  })
}
// 查询用户详细
export function getUser(userId, query) {
  return request({
    url: '/system/personnel/' + parseStrEmpty(userId),
    method: 'get',
  })
}
export function getUserAdd(query) {
  return request({
    url: '/system/personnel/',
    method: 'get',
    params: query
  })
}
// 新增用户
export function addUser(data) {
  return request({
    url: '/system/user',
    method: 'post',
    data: data
  })
}
// 新增员工用户
export function addEngineerUser(data) {
  return request({
    url: '/system/personnel',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/system/user',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(userId) {
  return request({
    url: '/system/user/' + userId,
    method: 'delete'
  })
}
// 删除服务商工程师
export function delEnginer(userId) {
  return request({
    url: `/system/user/` + userId,
    method: 'delete'
  })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password
  }
  return request({
    url: '/system/user/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  const data = {
    userId,
    status
  }
  return request({
    url: '/system/user/changeStatus',
    method: 'put',
    data: data
  })
}
// 用户能否登录
export function updateUserCanLogin(data) {
  return request({
    url: '/system/user/updateUserCanLogin',
    method: 'put',
    params: data
  })
}
// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/system/user/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function resetPwd(data) {
  return request({
    url: '/system/user/resetPwd',
    method: 'put',
    data: data
  })
}
// 用户密码重置
export function updateUserProfile(data) {
  return request({
    url: '/system/user/profile',
    method: 'put',
    data: data
  })
}
// 用户密码重置
export function batchResetPwd(id) {
  return request({
    url: '/system/user/batchResetPwd/' + id,
    method: 'put',
  })
}
// 用户密码修改
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/system/user/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    method: 'post',
    data: data
  })
}

// 查询授权角色
export function getAuthRole(query) {
  return request({
    url: '/system/user/authRole',
    method: 'get',
    params: query
  })
}

// 保存授权角色
export function updateAuthRole(data) {
  return request({
    url: '/system/user/authRole',
    method: 'put',
    params: data
  })
}
// 获取当前员工的任务信息
export function getUserMission(query) {
  return request({
    // url: `/workOrder/taskInfo/queryPageList?userId=${userId}&createTime=${createTime}`,
    url: `/workOrder/taskInfo/queryPageList`,
    method: 'get',
    params: query
  })
}
// 获取所有员工的任务日历信息
export function getAllUserTaskCalendar(query) {
  return request({
    url: `/workOrder/taskInfo/taskCalendar`,
    method: 'get',
    params: query
  })
}
// 获取所有员工的任务信息
export function getAllUserMission(query) {
  return request({
    url: `/workOrder/taskInfo/queryPageListNoTree`,
    method: 'get',
    params: query
  })
}
// 获取某个员工的任务信息
export function getAllUserMissions(userId) {
  return request({
    url: `/workOrder/taskInfo/queryPageListNoTree?userId=${userId}`,
    method: 'get'
  })
}


// 账号审核 角色选择
export function getUserType(type) {
  return request({
    url: '/system/role/list?type=' + type,
    method: 'get'
  })
}