import request from '@/utils/request'

// 查询证书管理列表
export function listCertificateManage(query) {
  return request({
    url: '/system/certificateManage/list',
    method: 'get',
    params: query
  })
}

// 查询证书管理详细
export function getCertificateManage(id) {
  return request({
    url: '/system/certificateManage/' + id,
    method: 'get'
  })
}

// 新增证书管理
export function addCertificateManage(data) {
  return request({
    url: '/system/certificateManage',
    method: 'post',
    data: data
  })
}

// 修改证书管理
export function updateCertificateManage(data) {
  return request({
    url: '/system/certificateManage',
    method: 'put',
    data: data
  })
}

// 删除证书管理
export function delCertificateManage(id) {
  return request({
    url: '/system/certificateManage/' + id,
    method: 'delete'
  })
}
