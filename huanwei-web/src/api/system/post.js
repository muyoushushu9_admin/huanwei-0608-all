import request from '@/utils/request'

// 查询岗位列表
export function listPost(query) {
  return request({
    url: '/system/job/list',
    method: 'get',
    params: query
  })
}
// 查询岗位下拉树结构
export function jobTreeselect(query) {
  return request({
    url: '/system/job/treeselect',
    method: 'get',
    params: query
  })
}
// 查询岗位下拉树结构
export function treeselectContainUser(containUser) {
  return request({
    url: '/system/job/treeselect?containUser=' + containUser,
    method: 'get'
  })
}
// 查询岗位列表（排除节点）
export function listDeptExcludeChild(JobId) {
  return request({
    url: '/system/job/list/exclude/' + JobId,
    method: 'get'
  })
}
// 查询岗位详细
export function getPost(jobId) {
  return request({
    url: '/system/job/' + jobId,
    method: 'get'
  })
}

// 新增岗位
export function addPost(data) {
  return request({
    url: '/system/job',
    method: 'post',
    data: data
  })
}

// 修改岗位
export function updatePost(data) {
  return request({
    url: '/system/job',
    method: 'put',
    data: data
  })
}

// 删除岗位
export function delPost(postId) {
  return request({
    url: '/system/job/' + postId,
    method: 'delete'
  })
}
