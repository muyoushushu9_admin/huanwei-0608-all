/* 编码规则部分接口 */
import request from '@/utils/request'

// 查询列表
export function encodeRuleList(query) {
  return request({
    url: '/system/encodeRule/list',
    method: 'get',
    params: query
  })
}
// 编辑
export function encodeRuleEdit(query) {
  return request({
    url: '/system/encodeRule/edit',
    method: 'put',
    data: query
  })
}
// 获取指定模块的编码规则
export function getByCode(query) {
  return request({
    url: '/system/encodeRule/getByCode',
    method: 'get',
    params: query
  })
}

