import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
  return request({
    url: '/system/role/list',
    method: 'get',
    params: query
  })
}

// 查询角色详细
export function getRole(roleId) {
  return request({
    url: '/system/role/' + roleId,
    method: 'get'
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/system/role',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateRole(data) {
  return request({
    url: '/system/role',
    method: 'put',
    data: data
  })
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: '/system/role/dataScope',
    method: 'put',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    roleId,
    status
  }
  return request({
    url: '/system/role/changeStatus',
    method: 'put',
    data: data
  })
}

// 删除角色
export function delRole(roleId) {
  return request({
    url: '/system/role/' + roleId,
    method: 'delete'
  })
}

// 查询角色已授权用户列表
export function allocatedUserList(query) {
  return request({
    url: '/system/role/authUser/allocatedList',
    method: 'get',
    params: query
  })
}

// 查询角色未授权用户列表
export function unallocatedUserList(query) {
  return request({
    url: '/system/role/authUser/unallocatedList',
    method: 'get',
    params: query
  })
}

// 取消用户授权角色
export function authUserCancel(data) {
  return request({
    url: '/system/role/authUser/cancel',
    method: 'put',
    data: data
  })
}

// 批量取消用户授权角色
export function authUserCancelAll(data) {
  return request({
    url: '/system/role/authUser/cancelAll',
    method: 'put',
    params: data
  })
}

// 授权用户选择
export function authUserSelectAll(data) {
  return request({
    url: '/system/role/authUser/selectAll',
    method: 'put',
    params: data
  })
}


//  用这个接口查角色下第一级 (弃用)
// export function menuList(query) {
//   return request({
//     url: '/system/menu/list',
//     method: 'get',
//     params: query
//   })
// }


//  用这个接口查角色下第一级
export function menuList(query) {
  return request({
    url: '/system/menu/selectableList',
    method: 'get',
    params: query
  })
}

//  这个查一级目录下的菜单
export function roleMenuTreeselect(query) {
  return request({
    url: '/system/menu/roleMenuTreeselect',
    method: 'get',
    params: query
  })
}


//  角色绑定自定义权限
export function customDataScope(data) {
  return request({
    url: '/system/user/customDataScope',
    method: 'put',
    data: data
  })
}


//  角色查询部门
export function userDeptTreeselect(userId) {
  return request({
    url: '/system/dept/userDeptTreeselect/' + userId,
    method: 'get'
  })
}

/**任务角色权限部分 */
// 获取表单和任务角色列表
export function taskPermissionsRole(query) {
  return request({
    url: '/taskPermissions/taskFormRole',
    method: 'get',
    params: query
  })
}
// 获取任务进度和操作权限数据
export function taskPermissions(query) {
  return request({
    url: '/taskPermissions/taskPermissions',
    method: 'get',
    params: query
  })
}
// 保存任务权限数据(单条)
export function saveTaskPermissions(data) {
  return request({
    url: '/taskPermissions',
    method: 'put',
    data: data
  })
}
// 保存任务权限数据(多条)
export function saveTaskPermissionsBatch(data) {
  return request({
    url: '/taskPermissions/batch',
    method: 'put',
    data: data
  })
}
