import request from "@/utils/request";

// 巡检
export function queryTableListIns(params) {
	return request({
		url: '/system/devPatrolInspectionTaskInfo/getPagePatrolInspectionTaskItemInfoByDeviceId',
		method: 'get',
        params
	})
}
// 点检
export function queryTableListSpt(params) {
	return request({
		url: '/system/devPointInspectionTaskInfo/getPagePointInspectionTaskItemInfoByDeviceId',
		method: 'get',
        params
	})
}
// 保养
export function queryTableListMa(params) {
	return request({
		url: '/template/devMaintain/getPageMaintainTaskItemInfoByDeviceId',
		method: 'get',
        params
	})
}