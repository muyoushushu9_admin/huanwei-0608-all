export function getFileType(fileType) {
    let docType = ''
    let fileTypesDoc = [
        // 'doc', 'docm', 'docx', 'dot', 'dotm', 'dotx', 'epub', 'fodt', 'htm', 'html', 'mht', 'odt', 'ott', 'pdf', 'rtf', 'txt', 'djvu', 'xps',
        'doc', 'docm', 'docx', 'docxf', 'dot', 'dotm', 'dotx', 'epub', 'fb2', 'fodt', 'htm', 'html', 'mht', 'mhtml', 'odt', 'oform', 'ott', 'rtf', 'stw', 'sxw', 'txt', 'wps', 'wpt', 'xml',
    ]
    let fileTypesCsv = [
        // 'csv', 'fods', 'ods', 'ots', 'xls', 'xlsm', 'xlsx', 'xlt', 'xltm', 'xltx',
        'csv', 'et', 'ett', 'fods', 'ods', 'ots', 'sxc', 'xls', 'xlsb', 'xlsm', 'xlsx', 'xlt', 'xltm', 'xltx', 'xml'
    ]
    let fileTypesPPt = [
        // 'fodp', 'odp', 'otp', 'pot', 'potm', 'potx', 'pps', 'ppsm', 'ppsx', 'ppt', 'pptm', 'pptx',
        'dps', 'dpt', 'fodp', 'odp', 'otp', 'pot', 'potm', 'potx', 'pps', 'ppsm', 'ppsx', 'ppt', 'pptm', 'pptx', 'sxi'
    ]
    let fileTypesPDF = [
        'pdf', 'djvu', 'xps', 'oxps'
    ]
    if (fileTypesDoc.includes(fileType)) {
        docType = 'text'
    }
    if (fileTypesCsv.includes(fileType)) {
        // docType = 'spreadsheet'
        docType = 'cell'
    }
    if (fileTypesPPt.includes(fileType)) {
        // docType = 'presentation'
        docType = 'slide'
    }
    if (fileTypesPDF.includes(fileType)) {
        docType = 'pdf'
    }
    return docType
}

