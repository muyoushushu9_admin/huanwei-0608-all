export default {
    model: null,
    taskModel: null,
    init(data) {
       this.model = data;
    },
    initTaskModel(task) {
        this.taskModel = task;
    },
    // 子任务暂停
    workSuspendSon(callback) {
        this.$modal.confirm('是否暂停子任务？', '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消'
        }).then(() => {
            this.loading = true;
            return workSuspendSon({ taskId: this.taskModel.id }).then(res => {
            this.$modal.msgSuccess('操作成功');
            if (this.taskModel.parentLayers == null || this.taskModel.parentLayers == "") {
                // 点击的是第一层
                this.taskParentId = this.taskModel.id; //父级id
                workOrderTaskInfoDetail(this.taskModel.id).then((response) => {
                    callback();
                });
            } else {
                if (this.taskModel.parentLayers.split(",").length == 1) {
                // 点击的是第二层
                this.taskParentId = this.taskModel.id; //父级id
                workOrderTaskInfoDetail(this.taskModel.id).then((response) => {
                    callback();
                });
                }
            }
            });
        });
        },
}