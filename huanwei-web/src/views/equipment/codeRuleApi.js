import request from '@/utils/request'
// 获得列表
export function getFeild(data) {
    return request({
      url: '/system/encode/list',
      method: 'get',
      params: data
    })
}
// 保存编码规则1
export function saveFeild(data) {
    return request({
      url: '/system/encode',
      method: 'put',
      data: data
    })
}