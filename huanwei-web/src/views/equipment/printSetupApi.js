import request from '@/utils/request'
// 获得列表
export function getData(data) {
    return request({
      url: 'system/labelRule/getRule',
      method: 'get',
      params: data
    })
}
// 保存编码规则1
export function saveData(data) {
    return request({
      url: '/system/labelRule',
      method: 'put',
      data: data
    })
}