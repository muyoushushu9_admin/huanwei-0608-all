import request from '@/utils/request'
// 查询工单
export function getOrderData(data) {
  return request({
    url: '/workOrder/taskInfo/typeStatistics',
    method: 'get',
    params: data
  })
}
// 查询异常项
export function getExceptionData(data) {
  return request({
    url: '/system/excItem/exceptionStatistics',
    method: 'get',
    params: data
  })
}


// 最近n个月
export function getDate(n) {
  var tomorrow = new Date()
  tomorrow.setTime(tomorrow.getTime() + 24 * 60 * 60 * 1000)
  var end = tomorrow
  var year = end.getFullYear()
  var month = end.getMonth() + 1 // 0-11表示 1-12月
  var day = end.getDate()
  var dateObj = {}
  dateObj.start = null
  dateObj.end = year + '-' + month + '-' + day
  var endMonthDay = new Date(year, month, 0).getDate()
  if (month - n <= 0) { // 如果是1、2、3月，年数往前推一年
    const start3MonthDay = new Date((year - 1), (12 - (n - parseInt(month))), 0).getDate() // 3个月前所在月的总天数
    if (start3MonthDay < day) { // 3个月前所在月的总天数小于现在的天日期
      dateObj.start = (year - 1) + '-' + (12 - (n - month)) + '-' + start3MonthDay
    } else {
      dateObj.start = (year - 1) + '-' + (12 - (n - month)) + '-' + day
    }
  } else {
    const start3MonthDay = new Date(year, (parseInt(month) - n), 0).getDate() // 3个月前所在月的总天数
    if (start3MonthDay < day) { // 3个月前所在月的总天数小于现在的天日期
      if (day < endMonthDay) { // 当前天日期小于当前月总天数,2月份比较特殊的月份
        dateObj.start = year + '-' + (month - n) + '-' + (start3MonthDay - (endMonthDay - day))
      } else {
        dateObj.start = year + '-' + (month - n) + '-' + start3MonthDay
      }
    } else {
      dateObj.start = year + '-' + (month - n) + '-' + day
    }
  }
  const newThreeMonthDate = [dateObj.start + ' ' + '00:00:00', dateObj.end + ' ' + '00:00:00']

  return newThreeMonthDate
}

export function getDate1(n) {
  var dd = new Date();
  // dd.setDate(dd.getDate() + n); //获取n天后的日期
  var y = dd.getFullYear();
  var m = dd.getMonth() - (n - 1); //获取当前月份的日期
  var m1 = dd.getMonth() + 1;
  var d = dd.getDate();
  if (m <= 9) {
    m = `0${m}`;
  }
  if (m1 <= 9) {
    m1 = `0${m}`;
  }
  let day2 = y + '-' + m1 + '-' + d;
  let day1 = y + '-' + m + '-' + d;
  return [day1, day2];
};
