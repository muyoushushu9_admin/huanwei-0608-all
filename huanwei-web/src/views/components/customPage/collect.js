import request from '@/utils/request'
// 保存收藏
export function saveCollect(data) {
    return request({
      url: '/system/collect',
      method: 'post',
      data: data
    })
}
// 查询收藏
export function getCollect(data) {
    return request({
      url: '/system/collect/getList',
      method: 'get',
      data: data
    })
}
// 查询异常项
export function getExceptionItem(data) {
  return request({
    url: '/system/kanbanConfig/excItemData',
    method: 'get',
    data: data
  })
}
// 查询服务请求
export function getOrderItem(data) {
  return request({
    url: '/system/kanbanConfig/serviceRequestData',
    method: 'get',
    data: data
  })
}